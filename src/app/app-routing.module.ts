import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomePageComponent} from './shared/components/global/home-page/home-page.component';
import {NotFoundComponent} from './shared/components/global/not-found/not-found.component';
import {LegalMentionsPageComponent} from './shared/components/global/legal-mentions-page/legal-mentions-page.component';
import {WebContentByIdComponent} from './shared/components/visitor/web_content/web-content-by-id/web-content-by-id.component';
import {GetWebContentsComponent} from './shared/components/visitor/web_content/get-web-contents/get-web-contents.component';
import {SignupComponent} from './shared/components/signup_login/signup/signup.component';
import {LoginComponent} from './shared/components/signup_login/login/login.component';
import {PasswordResetComponent} from './shared/components/signup_login/password-reset/password-reset.component';
import {EmailValidationComponent} from './shared/components/signup_login/email-validation/email-validation.component';
import {UserAccountComponent} from './shared/components/user/user-account/user-account.component';

const routes: Routes = [

  {path: 'mentions-legales', component: LegalMentionsPageComponent},
  {path: 'publications/:title/:id', component: WebContentByIdComponent},

  {path: 'publications/tri/:sort', component: GetWebContentsComponent},

  {path: 'inscription', component: SignupComponent},
  {path: 'inscription/validation-mail/:token', component: EmailValidationComponent},

  {path: 'connexion', component: LoginComponent},
  {path: 'connexion/reinitialisation-mot-de-passe/:token', component: PasswordResetComponent},


  {path: 'utilisateur/compte/:idUser', component: UserAccountComponent},


  {path: 'publications/type/:typeTitle/:typeId', component: GetWebContentsComponent},
  {path: 'publications/type/:typeTitle/:typeId/tri/:sort', component: GetWebContentsComponent},

  {path: 'publications/categorie/:catTitle/:catId', component: GetWebContentsComponent},
  {path: 'publications/categorie/:catTitle/:catId/tri/:sort', component: GetWebContentsComponent},

  {path: 'publications/type/:typeTitle/:typeId/categorie/:catTitle/:catId', component: GetWebContentsComponent},
  {path: 'publications/type/:typeTitle/:typeId/categorie/:catTitle/:catId/tri/:sort', component: GetWebContentsComponent},

  {path: 'publications/utilisateur/:userTitle/:userId', component: GetWebContentsComponent},
  {path: 'publications/utilisateur/:userTitle/:userId/tri/:sort', component: GetWebContentsComponent},

  {path: 'publications/utilisateur/:userTitle/:userId/type/:typeTitle/:typeId', component: GetWebContentsComponent},
  {path: 'publications/utilisateur/:userTitle/:userId/type/:typeTitle/:typeId/tri/:sort', component: GetWebContentsComponent},

  {path: 'publications/utilisateur/:userTitle/:userId/categorie/:catTitle/:catId', component: GetWebContentsComponent},
  {path: 'publications/utilisateur/:userTitle/:userId/categorie/:catTitle/:catId/tri/:sort', component: GetWebContentsComponent},


  {
    path: 'publications/utilisateur/:userTitle/:userId/type/:typeTitle/:typeId/categorie/:catTitle/:catId',
    component: GetWebContentsComponent
  },
  {
    path: 'publications/utilisateur/:userTitle/:userId/type/:typeTitle/:typeId/categorie/:catTitle/:catId/tri/:sort',
    component: GetWebContentsComponent
  },


  {path: '404', component: NotFoundComponent},
  {path: '', component: HomePageComponent},
  {path: '**', redirectTo: '/404'}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
