import {Component, OnDestroy} from '@angular/core';
import {VisitorWebContentTypeService} from './core/services/visitor/visitor-web-content-type.service';
import {NavigationEnd, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {UnsubscribeService} from './shared/utilities/unsubscribe.service';
import {VisitorWebContentCategoryService} from './core/services/visitor/visitor-web-content-category.service';
import {WebContentType} from './shared/models/dto/web-content-type';
import {WebContentCategory} from './shared/models/dto/web-content-category';
import {TokenStorageService} from './core/services/signup_login/token-storage.service';
import {LoginJwtResponse} from './shared/models/payload/response/login/login-jwt-response';
import {UserService} from './core/services/user/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy {

  title = 'cms-front';

  webContentTypes: Array<WebContentType> | undefined;
  categories: Array<WebContentCategory> | undefined;
  sessionUser: LoginJwtResponse | undefined;


  subscriptions = new Array<Subscription>();

  constructor(private visitorWebContentTypeService: VisitorWebContentTypeService,
              private visitorWebContentCategoryService: VisitorWebContentCategoryService,
              private tokenStorageService: TokenStorageService,
              private userService: UserService,
              private router: Router) {

    router.events.subscribe(r => {

      if (r instanceof NavigationEnd) {

        this.sessionUser = this.tokenStorageService.getUser();

        this.subscriptions.push(
          // get main WebContentTypes, displayed as navbar items
          this.visitorWebContentTypeService.getNavbarTypes()
            .subscribe(value => {
              this.webContentTypes = value;

            }, error => {
            }));


        this.subscriptions.push(
          this.visitorWebContentCategoryService
            .getNavbarCategories()
            .subscribe(value2 => {
              this.categories = value2;
            }, error2 => {
            }));
      }
    });
  }

  ngOnDestroy(): void {
    UnsubscribeService.unsubscribeAll(this.subscriptions);
  }
}
