import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {UserSignupRequest} from '../../../shared/models/payload/request/signup_login/user-signup-request';
import {Observable} from 'rxjs';
import {VisitorApiUrls} from '../../../shared/models/constants/visitor-api-urls';

@Injectable({
  providedIn: 'root'
})
export class SignupService {

  private signupApiUrl = VisitorApiUrls.publicBaseUrl + VisitorApiUrls.signupUrl;
  private signupEmailValidationApiUrl = VisitorApiUrls.publicBaseUrl + VisitorApiUrls.signupEmailValidationUrl;


  constructor(private http: HttpClient) {
  }

  signup(request: UserSignupRequest): Observable<any> {
    return this.http.post(`${this.signupApiUrl}`, request);
  }

  emailValidation(token: string): Observable<any> {
    return this.http.post(`${this.signupEmailValidationApiUrl}`, token);
  }


}
