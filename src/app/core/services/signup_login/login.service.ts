import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {VisitorApiUrls} from '../../../shared/models/constants/visitor-api-urls';
import {LoginRequest} from '../../../shared/models/payload/request/signup_login/login-request';
import {Observable} from 'rxjs';
import {PasswordResetRequest} from '../../../shared/models/payload/request/signup_login/password-reset-request';

@Injectable({
  providedIn: 'root'
})
export class LoginService {


  private loginApiUrl = VisitorApiUrls.publicBaseUrl + VisitorApiUrls.loginUrl;
  private sendPasswordResetEmailApiUrl = VisitorApiUrls.publicBaseUrl + VisitorApiUrls.sendPasswordResetEmailUrl;
  private resetPasswordApiUrl = VisitorApiUrls.publicBaseUrl + VisitorApiUrls.resetPasswordUrl;


  constructor(private http: HttpClient) {
  }

  login(request: LoginRequest): Observable<any> {
    return this.http.post(`${this.loginApiUrl}`, request);
  }

  sendPasswordResetEmail(email: string): Observable<any> {
    return this.http.post(`${this.sendPasswordResetEmailApiUrl}`, {email});
  }

  resetPassword(request: PasswordResetRequest): Observable<any> {
    return this.http.post(`${this.resetPasswordApiUrl}`, request);
  }


}
