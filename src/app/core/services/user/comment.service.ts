import {Injectable} from '@angular/core';
import {UserApiUrls} from '../../../shared/models/constants/user-api-urls';
import {HttpClient} from '@angular/common/http';
import {AddCommentRequest} from '../../../shared/models/payload/request/user/add-comment-request';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommentService {


  private addCommentApiUrl = UserApiUrls.publicBaseUrl + UserApiUrls.userAddCommentApiUrl;


  constructor(private http: HttpClient) {
  }


  addComment(request: AddCommentRequest): Observable<any> {
    return this.http.post(this.addCommentApiUrl, request);
  }


}
