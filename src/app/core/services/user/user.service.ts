import {Injectable} from '@angular/core';
import {UserApiUrls} from '../../../shared/models/constants/user-api-urls';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ModifyUserBaseInfoRequest} from '../../../shared/models/payload/request/user/modify-user-base-info-request';
import {ModifyEmailAddressRequest} from '../../../shared/models/payload/request/user/modify-email-address-request';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private getUserDetailsApiUrl = UserApiUrls.publicBaseUrl + UserApiUrls.userGetUserApiUrl;
  private modifyUserBaseInfoApiUrl = UserApiUrls.publicBaseUrl + UserApiUrls.userModifyBaseInfoApiUrl;
  private resendValidationMailApiUrl = UserApiUrls.publicBaseUrl + UserApiUrls.userResendValidationMailApiUrl;
  private changeMailApiUrl = UserApiUrls.publicBaseUrl + UserApiUrls.userModifyMailApiUrl;





  constructor(private http: HttpClient) {
  }

  getLoggedInUser(): Observable<any> {
    return this.http.get(this.getUserDetailsApiUrl);
  }

  modifyBaseInfo(request: ModifyUserBaseInfoRequest): Observable<any> {
    return this.http.post(this.modifyUserBaseInfoApiUrl, request);
  }

  resendValidationMail(): Observable<any> {
    return this.http.get(this.resendValidationMailApiUrl);
  }


  modifyEmailAddress(request: ModifyEmailAddressRequest): Observable<any> {
    return this.http.post(this.changeMailApiUrl, request);
  }


}
