import {Injectable} from '@angular/core';
import {VisitorApiUrls} from '../../../shared/models/constants/visitor-api-urls';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VisitorWebContentTypeService {


  private getWebContentSortingTypesApiUrl = VisitorApiUrls.publicBaseUrl + '/public/visitor/web-content-type/get-web-content-sorting-types';
  private getTypeByVersionIdApiUrl = VisitorApiUrls.publicBaseUrl + '/public/visitor/web-content-type/et-by-version-id/';
  private getNavbarTypesApiUrl = VisitorApiUrls.publicBaseUrl + '/public/visitor/web-content-type/get-navbar-types';
  private getFooterTypesApiUrl = VisitorApiUrls.publicBaseUrl + '/public/visitor/web-content-type/get-footer-types';


  constructor(private http: HttpClient) {
  }


  getTypeByVersionId(versionId: number): Observable<any> {
    return this.http.get(this.getTypeByVersionIdApiUrl + versionId);
  }

  getWebContentSortingTypes(): Observable<any> {
    return this.http.get(this.getWebContentSortingTypesApiUrl);
  }

  getNavbarTypes(): Observable<any> {
    return this.http.get(this.getNavbarTypesApiUrl);
  }

  getFooterTypes(): Observable<any> {
    return this.http.get(this.getFooterTypesApiUrl);
  }


}
