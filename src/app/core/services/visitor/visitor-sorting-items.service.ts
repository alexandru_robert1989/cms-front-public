import {Injectable} from '@angular/core';
import {VisitorApiUrls} from '../../../shared/models/constants/visitor-api-urls';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VisitorSortingItemsService {


  private getOrderSortersApiUrl = VisitorApiUrls.publicBaseUrl + '/public/visitor/sorting-items/get-order-sorters';


  constructor(private http: HttpClient) {
  }


  getOrderSortersItems(): Observable<any> {
    return this.http.get(this.getOrderSortersApiUrl);
  }


}
