import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {VisitorApiUrls} from '../../../shared/models/constants/visitor-api-urls';

@Injectable({
  providedIn: 'root'
})
export class VisitorWebContentCategoryService {


  private getByVersionIdApiUrl = VisitorApiUrls.publicBaseUrl + '/public/visitor/web-content-category/get-by-version-id/';
  private getWebContentSortingCategoriesApiUrl = VisitorApiUrls.publicBaseUrl + '/public/visitor/web-content-category/get-web-content-sorting-categories';
  private getNavbarCategoriesApiUrl = VisitorApiUrls.publicBaseUrl + '/public/visitor/web-content-category/get-navbar-categories';


  constructor(private http: HttpClient) {
  }


  getByVersionId(versionId: number): Observable<any> {
    return this.http.get(this.getByVersionIdApiUrl + versionId);

  }

  getWebContentSortingCategories(): Observable<any> {
    return this.http.get(this.getWebContentSortingCategoriesApiUrl);
  }

  getNavbarCategories(): Observable<any> {
    return this.http.get(this.getNavbarCategoriesApiUrl);
  }


}
