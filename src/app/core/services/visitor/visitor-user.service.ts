import {Injectable} from '@angular/core';
import {VisitorApiUrls} from '../../../shared/models/constants/visitor-api-urls';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VisitorUserService {


  private getEditorsApiUrl = VisitorApiUrls.publicBaseUrl + '/public/visitor/users/editors';
  private getEditorByWebContentVersionIdApiUrl = VisitorApiUrls.publicBaseUrl + '/public/visitor/users/by-web-content';

  constructor(private http: HttpClient) {
  }


  getEditors(): Observable<any> {
    return this.http.get(this.getEditorsApiUrl);
  }

  getEditorById(id: number): Observable<any> {
    return this.http.get(this.getEditorsApiUrl + '/' + id);
  }

  getEditorByWebContentId(id: number): Observable<any> {
    return this.http.get(this.getEditorByWebContentVersionIdApiUrl + '/' + id);
  }


}
