import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {VisitorApiUrls} from '../../../shared/models/constants/visitor-api-urls';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VisitorCommentService {



  private getWebContentParentCommentsUrl = VisitorApiUrls.publicBaseUrl + '/public/visitor/comments/parent-comments/';
  private getWebContentChildrenCommentsUrl = VisitorApiUrls.publicBaseUrl + '/public/visitor/comments/children-comments/';


  constructor(private http: HttpClient) {
  }


  getWebContentParentComments(webContentVersionId: string | number | undefined): Observable<any> {
    return this.http.get(this.getWebContentParentCommentsUrl + webContentVersionId);
  }

  getWebContentChildrenComments(parentCommentId: string | number | undefined): Observable<any> {
    return this.http.get(this.getWebContentChildrenCommentsUrl + parentCommentId);
  }




}
