import {Injectable} from '@angular/core';
import {VisitorApiUrls} from '../../../shared/models/constants/visitor-api-urls';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VisitorWebContentService {


  private webContentApiUrl = VisitorApiUrls.publicBaseUrl + VisitorApiUrls.visitorWebContentUrl;

  private webContentGetOneByVersionIdApiUrl = VisitorApiUrls.publicBaseUrl + '/public/visitor/web-content/get-one/';
  private webContentGetAllApiUrl = VisitorApiUrls.publicBaseUrl + '/public/visitor/web-content/get-all/';
  private webContentGetAllByTypeApiUrl = VisitorApiUrls.publicBaseUrl + '/public/visitor/web-content/get-all-by-type/';
  private webContentGetAllByCategoryApiUrl = VisitorApiUrls.publicBaseUrl + '/public/visitor/web-content/get-all-by-category/';
  private webContentGetAllByEditorApiUrl = VisitorApiUrls.publicBaseUrl + '/public/visitor/web-content/get-all-by-editor/';
  private webContentGetAllByTypeAndCategoryApiUrl = VisitorApiUrls.publicBaseUrl + '/public/visitor/web-content/get-all-by-type-and-category/';
  private webContentGetAllByTypeAndUserApiUrl = VisitorApiUrls.publicBaseUrl + '/public/visitor/web-content/get-all-by-type-and-user/';
  private webContentGetAllByCategoryAndUserApiUrl = VisitorApiUrls.publicBaseUrl + '/public/visitor/web-content/get-all-by-category-and-user/';
  private webContentGetAllByTypaAndCategoryAndUserApiUrl = VisitorApiUrls.publicBaseUrl + '/public/visitor/web-content/get-all-by-type-and-category-and-user/';


  private webContentHomePageApiUrl = VisitorApiUrls.publicBaseUrl + VisitorApiUrls.visitorWebContentHomePageUrl;
  private webContentLegalMentionsPageApiUrl = VisitorApiUrls.publicBaseUrl + VisitorApiUrls.visitorWebContentLegalMentionsPageUrl;


  constructor(private http: HttpClient) {
  }

  getHomePage(): Observable<any> {
    return this.http.get(this.webContentHomePageApiUrl);
  }

  getLegalMentionsPage(): Observable<any> {
    return this.http.get(this.webContentLegalMentionsPageApiUrl);
  }

  getOneByVersionId(versionId: number): Observable<any> {
    return this.http.get(this.webContentGetOneByVersionIdApiUrl + versionId);
  }

  getAllByType(typeVersionId: number, sort: string | undefined): Observable<any> {
    sort = this.getSortValue(sort);
    return this.http.get(this.webContentGetAllByTypeApiUrl + typeVersionId + '/' + sort);
  }

  getAllByCategory(categoryVersionId: number, sort: string | undefined): Observable<any> {
    sort = this.getSortValue(sort);
    return this.http.get(this.webContentGetAllByCategoryApiUrl + categoryVersionId + '/' + sort);
  }


  getAllByEditor(userId: number, sort: string | undefined): Observable<any> {
    sort = this.getSortValue(sort);
    return this.http.get(this.webContentGetAllByEditorApiUrl + userId + '/' + sort);
  }


  getAllByTypeAndCategory(typeVersionId: number, categoryVersionId: number, sort: string | undefined): Observable<any> {
    sort = this.getSortValue(sort);
    return this.http.get(this.webContentGetAllByTypeAndCategoryApiUrl + typeVersionId + '/' + categoryVersionId + '/' + sort);
  }

  getAllByTypeAndUser(typeVersionId: number, userId: number, sort: string | undefined): Observable<any> {
    sort = this.getSortValue(sort);
    return this.http.get(this.webContentGetAllByTypeAndUserApiUrl + typeVersionId + '/' + userId + '/' + sort);
  }


  getAllByCategoryAndUser(categoryVersionId: number, userId: number, sort: string | undefined): Observable<any> {
    sort = this.getSortValue(sort);
    return this.http.get(this.webContentGetAllByCategoryAndUserApiUrl + categoryVersionId + '/' + userId + '/' + sort);
  }


  getAllByTypeAndCategoryAndUser(typeVersionId: number,
                                 categoryVersionId: number,
                                 userId: number, sort: string | undefined): Observable<any> {
    sort = this.getSortValue(sort);
    return this.http.get
    (this.webContentGetAllByTypaAndCategoryAndUserApiUrl + typeVersionId + '/' + categoryVersionId + '/' + userId + '/' + sort);
  }


  getAllSortedByDefault(): Observable<any> {
    return this.http.get(this.webContentGetAllApiUrl);
  }

  getAllSorted(sort: string): Observable<any> {
    return this.http.get(this.webContentGetAllApiUrl + sort);
  }


  getWebContentsFromCompleteRequest(typeVersionId: number | undefined,
                                    categoryVersionId: number | undefined,
                                    userId: number | undefined,
                                    sort: string | undefined): Observable<any> | undefined {


    if ((typeVersionId !== undefined && !Number.isNaN(typeVersionId)) &&
      (categoryVersionId !== undefined && !Number.isNaN(categoryVersionId))
      && (userId !== undefined && !Number.isNaN(userId))) {
      return this.getAllByTypeAndCategoryAndUser(typeVersionId, categoryVersionId, userId, sort);
    } else if ((typeVersionId !== undefined && !Number.isNaN(typeVersionId)) &&
      (categoryVersionId !== undefined && !Number.isNaN(categoryVersionId))
      && (userId === undefined || Number.isNaN(userId))) {
      return this.getAllByTypeAndCategory(typeVersionId, categoryVersionId, sort);
    } else if ((typeVersionId !== undefined && !Number.isNaN(typeVersionId)) &&
      (categoryVersionId === undefined || Number.isNaN(categoryVersionId))
      && (userId !== undefined && !Number.isNaN(userId))) {
      return this.getAllByTypeAndUser(typeVersionId, userId, sort);
    } else if ((typeVersionId === undefined || Number.isNaN(typeVersionId)) &&
      (categoryVersionId !== undefined && !Number.isNaN(categoryVersionId))
      && (userId !== undefined && !Number.isNaN(userId))) {
      return this.getAllByCategoryAndUser(categoryVersionId, userId, sort);
    } else if ((typeVersionId !== undefined && !Number.isNaN(typeVersionId)) &&
      (categoryVersionId === undefined || Number.isNaN(categoryVersionId))
      && (userId !== undefined || Number.isNaN(userId))) {
      return this.getAllByType(typeVersionId, sort);
    } else if ((typeVersionId === undefined || Number.isNaN(typeVersionId)) &&
      (categoryVersionId !== undefined && !Number.isNaN(categoryVersionId))
      && (userId === undefined || Number.isNaN(userId))) {
      return this.getAllByCategory(categoryVersionId, sort);
    } else if ((typeVersionId === undefined || Number.isNaN(typeVersionId)) &&
      (categoryVersionId === undefined || Number.isNaN(categoryVersionId))
      && (userId !== undefined && !Number.isNaN(userId))) {
      return this.getAllByEditor(userId, sort);
    }

    return undefined;

  }


  private getSortValue(sort: string | undefined): string {
    if (sort === undefined || sort === '') {
      return 'default';
    }
    return sort;
  }


}
