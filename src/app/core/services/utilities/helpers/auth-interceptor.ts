import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {HttpInterceptor, HttpHandler, HttpRequest} from '@angular/common/http';
import {TokenStorageService} from '../../signup_login/token-storage.service';

/**
 * HttpInterceptor intercept() inspects
 * transform HTTP requests before sending them to the server.
 */
const TOKEN_HEADER_KEY = 'Authorization';       // for Spring Boot back-end

@Injectable()
export class AuthInterceptor implements HttpInterceptor {


  constructor(private token: TokenStorageService) {
  }


  /**
   * intercept() gets HTTPRequest object, change it and forward to HttpHandler
   * object’s handle() method. It transforms HTTPRequest object into an Observable<HttpEvents>.
   * next: HttpHandler object represents the next interceptor in the chain of interceptors.
   * he final ‘next’ in the chain is the Angular HttpClient.
   * @param req : HttpRequest
   * @param next : HttpHandler
   */
  intercept(req: HttpRequest<any>,
            next: HttpHandler): any {
    let authReq = req;
    const token = this.token.getToken(); // token récupéré de la session
    if (token) {
      authReq = req.clone({headers: req.headers.set(TOKEN_HEADER_KEY, 'Bearer ' + token)});
    }
    return next.handle(authReq);
  }
}

export const authInterceptorProviders = [
  {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}
];
