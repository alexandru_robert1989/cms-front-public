import {Injectable} from '@angular/core';
import {Router} from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class VisitorRoutingService {

  routing: boolean | undefined;

  constructor(private router: Router) {
  }


  goTo(chemin: string): any {
    this.router.navigate([chemin]);
  }

  goToSort(sortId: string | undefined): any {
    if (sortId) {
      this.routing = true;
      this.router.navigate([`publications/tri/${sortId}`]);
    }
  }


  goToContent(webContentVersionId: string | number | undefined, title: string | undefined): any {
    this.routing = true;
    this.router.navigate([`publications/${title}/${webContentVersionId}`]);

  }


  goToType(typeVersionId: string | number | undefined, title: string | undefined): any {
    if (typeVersionId === undefined && title === undefined) {
      return;
    }

    this.routing = true;
    this.router.navigate([`publications/type/${title}/${typeVersionId}`]);
  }

  goToTypeAndSort(typeVersionId: string | number | undefined, typeTitle: string | undefined, sort: string | undefined): any {
    if (typeVersionId && typeTitle && sort) {
      this.routing = true;
      this.router.navigate([`publications/type/${typeTitle}/${typeVersionId}/tri/${sort}`]);
    }
  }


  goToCategory(catVersionId: string | number | undefined, catTitle: string | undefined): any {
    if (catVersionId && catTitle) {
      this.routing = true;
      this.router.navigate([`publications/categorie/${catTitle}/${catVersionId}`]);
    } else {
      return;
    }
  }

  goToCategoryAndSort(catVersionId: string | number | undefined, title: string | undefined, sort: string | undefined): any {
    if (catVersionId && title && sort) {
      this.routing = true;
      this.router.navigate([`publications/categorie/${title}/${catVersionId}/tri/${sort}`]);
    }

  }

  goToTypeAndCategory(typeVersionId: string | number | undefined, typeTitle: string | undefined,
                      catVersionId: string | number | undefined, catTitle: string | undefined): any {
    if (typeVersionId && typeTitle && catTitle && catTitle) {
      this.routing = true;
      this.router.navigate([`publications/type/${typeTitle}/${typeVersionId}/categorie/${catTitle}/${catVersionId}`]);
    }

  }

  goToTypeAndCategoryAndSort(typeVersionId: string | number | undefined, typeTitle: string | undefined,
                             catVersionId: string | number | undefined, catTitle: string | undefined,
                             sort: string | undefined): any {
    if (typeVersionId && typeTitle && catTitle && catTitle && sort) {
      this.routing = true;
      this.router.navigate([`publications/type/${typeTitle}/${typeVersionId}/categorie/${catTitle}/${catVersionId}/tri/${sort}`]);
    }

  }


  goToUserContents(userId: string | number | undefined, title: string | undefined): any {
    if (userId && title) {
      this.routing = true;
      this.router.navigate([`publications/utilisateur/${title}/${userId}`]);
    }

  }

  goToUserContentsAndSort(userId: string | number | undefined, userTitle: string | undefined, sort: string | undefined): any {
    this.routing = true;
    this.router.navigate([`publications/utilisateur/${userTitle}/${userId}/tri/${sort}`]);
  }

  goToUserAndType(userId: string | number | undefined, userTitle: string | undefined,
                  typeVersionId: string | number | undefined, typeTitle: string | undefined): any {
    this.routing = true;
    this.router.navigate([`publications/utilisateur/${userTitle}/${userId}/type/${typeTitle}/${typeVersionId}`]);
  }

  goToUserAndTypeAndSort(userId: string | number | undefined, userTitle: string | undefined,
                         typeVersionId: string | number | undefined, typeTitle: string | undefined,
                         sort: string | undefined): any {
    this.routing = true;
    this.router.navigate([`publications/utilisateur/${userTitle}/${userId}/type/${typeTitle}/${typeVersionId}/tri/${sort}`]);
  }


  goToUserAndCategory(userId: string | number | undefined, userTitle: string | undefined,
                      catVersionId: string | number | undefined, catTitle: string): any {
    this.routing = true;
    this.router.navigate([`publications/utilisateur/${userTitle}/${userId}/categorie/${catTitle}/${catVersionId}`]);
  }

  goToUserAndCategoryAndSort(userId: string | number | undefined, userTitle: string | undefined,
                             catVersionId: string | number | undefined, catTitle: string | undefined,
                             sort: string | undefined): any {
    this.routing = true;
    this.router.navigate([`publications/utilisateur/${userTitle}/${userId}/categorie/${catTitle}/${catVersionId}/tri/${sort}`]);
  }


  goToUserAndTypeAndCategory(userId: string | number | undefined, userTitle: string | undefined,
                             typeVersionId: string | number | undefined, typeTitle: string | undefined,
                             catVersionId: string | number | undefined, catTitle: string): any {
    this.routing = true;
    this.router.navigate([`publications/utilisateur/${userTitle}/${userId}/type/${typeTitle}/${typeVersionId}/categorie/${catTitle}/${catVersionId}`]);
  }


  goToUserAndTypeAndCategoryAndSort(userId: string | number | undefined, userTitle: string | undefined,
                                    typeVersionId: string | number | undefined, typeTitle: string | undefined,
                                    catVersionId: string | number | undefined, catTitle: string | undefined,
                                    sort: string | undefined): any {
    this.routing = true;
    this.router.navigate([`publications/utilisateur/${userTitle}/${userId}/type/${typeTitle}/${typeVersionId}/categorie/${catTitle}/${catVersionId}/tri/${sort}`]);
  }


}
