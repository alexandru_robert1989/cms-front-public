import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {SafePipe} from './shared/pipes/safe.pipe';
import {HomePageComponent} from './shared/components/global/home-page/home-page.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NotFoundComponent} from './shared/components/global/not-found/not-found.component';
import {LegalMentionsPageComponent} from './shared/components/global/legal-mentions-page/legal-mentions-page.component';
import {WebContentByIdComponent} from './shared/components/visitor/web_content/web-content-by-id/web-content-by-id.component';
import {NavbarComponent} from './shared/components/global/navbar/navbar.component';
import {FooterComponent} from './shared/components/global/footer/footer.component';
import { GetWebContentsComponent } from './shared/components/visitor/web_content/get-web-contents/get-web-contents.component';
import { LoadingComponent } from './shared/components/global/loading/loading.component';
import { SignupComponent } from './shared/components/signup_login/signup/signup.component';
import { LoginComponent } from './shared/components/signup_login/login/login.component';
import { PasswordResetComponent } from './shared/components/signup_login/password-reset/password-reset.component';
import { EmailValidationComponent } from './shared/components/signup_login/email-validation/email-validation.component';
import {authInterceptorProviders} from './core/services/utilities/helpers/auth-interceptor';
import { UserAccountComponent } from './shared/components/user/user-account/user-account.component';
import { BaseInfoFormComponent } from './shared/components/user/base-info-form/base-info-form.component';
import { ModifyEmailFormComponent } from './shared/components/user/modify-email-form/modify-email-form.component';
import { VisitorSortWebContentsComponent } from './shared/components/visitor/web_content/visitor-sort-web-contents/visitor-sort-web-contents.component';
import { AddCommentComponent } from './shared/components/user/add-comment/add-comment.component';
import { ResendValidationEmailComponent } from './shared/components/user/resend-validation-email/resend-validation-email.component';
import { CommentsComponent } from './shared/components/visitor/comments/comments.component';
import { ContactComponent } from './shared/components/user/contact/contact.component';

@NgModule({
  declarations: [
    AppComponent,
    SafePipe,
    HomePageComponent,
    NotFoundComponent,
    LegalMentionsPageComponent,
    WebContentByIdComponent,
    NavbarComponent,
    FooterComponent,
    GetWebContentsComponent,
    LoadingComponent,
    SignupComponent,
    LoginComponent,
    PasswordResetComponent,
    EmailValidationComponent,
    UserAccountComponent,
    BaseInfoFormComponent,
    ModifyEmailFormComponent,
    VisitorSortWebContentsComponent,
    AddCommentComponent,
    ResendValidationEmailComponent,
    CommentsComponent,
    ContactComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,

  ],
  providers: [authInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule {
}
