export class VisitorApiUrls {


  // @ts-ignore
  public static publicBaseUrl = window['cfgApiBaseUrl'];

  public static visitorWebContentUrl = '/public/visitor/web-content/get-from-request';
  public static visitorWebContentHomePageUrl = '/public/visitor/web-content/home-page';
  public static visitorWebContentLegalMentionsPageUrl = '/public/visitor/web-content/legal-mentions';

  public static visitorSortingItemsUrl = '/public/visitor/sorting-items/get-from-request';


  public static signupUrl = '/public/signup/signup';
  public static signupEmailValidationUrl = '/public/signup/email-validation';

  public static loginUrl = '/public/login/login';
  public static sendPasswordResetEmailUrl = '/public/login/send-password-reset-email';
  public static resetPasswordUrl = '/public/login/reset-password';


}
