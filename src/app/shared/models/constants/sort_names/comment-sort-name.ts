export class CommentSortName {

  public static TOTAL_COMMENTS_ASC = 'total-comments-asc';
  public static TOTAL_COMMENTS_DESC = 'total-comments-desc';

}
