export class SizeSortName {

  public static SIZE_ASC = 'size-asc';

  public static SIZE_DESC = 'size-desc';

}
