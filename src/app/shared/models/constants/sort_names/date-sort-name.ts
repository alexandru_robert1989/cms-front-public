export class DateSortName {


  public static ASC_CREATION_DATE = 'asc-creation-date';
  public static DESC_CREATION_DATE = 'desc-creation-date';


  public static ASC_PUBLISHING_DATE = 'asc-publishing-date';
  public static DESC_PUBLISHING_DATE = 'desc-publishing-date';


  public static ASC_EDITION_DATE = 'asc-edition-date';
  public static DESC_EDITION_DATE = 'desc-edition-date';


  public static ASC_SUPPRESSION_DATE = 'asc-suppression-date';
  public static DESC_SUPPRESSION_DATE = 'desc-suppression-date';


  public static ASC_SIGNUP_DATE = 'asc_signup-date';
  public static DESC_SIGNUP_DATE = 'desc_signup-date';
}
