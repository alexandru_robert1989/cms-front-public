export class TitleSortName {

  public static ASC_TITLE = 'asc-title';

  public static DESC_TITLE = 'desc-title';

}
