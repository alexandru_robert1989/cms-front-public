export class TotalWebContentsSortName {

  public static TOTAL_WEB_CONTENTS_ASC = 'total-web-contents-asc';

  public static TOTAL_WEB_CONTENTS_DESC = 'total-web-contents-desc';

}
