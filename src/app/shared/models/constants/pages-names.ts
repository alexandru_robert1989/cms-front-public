export class PagesNames {

  public static homePage = 'Accueil';
  public static legalMentionsPage = 'Charte d\'utilisation du site';
  public static indicationsPage = 'Indications';

}
