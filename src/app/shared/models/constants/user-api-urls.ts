export class UserApiUrls {

  // @ts-ignore
  public static publicBaseUrl = window['cfgApiBaseUrl'];

  public static userGetUserApiUrl = '/auth/user/get-session-user';
  public static userModifyBaseInfoApiUrl = '/auth/user/modify-base-info';
  public static userResendValidationMailApiUrl = '/auth/user/resend-validation-mail';
  public static userModifyMailApiUrl = '/auth/user/modify-email-address';
  public static userAddCommentApiUrl = '/auth/user/comments/add-comment';


}
