export class PasswordResetRequest {
  token: string | undefined;
  newPassword: string;
  newPasswordConfirmation: string;

  constructor(token: string | undefined,
              newPassword: string,
              newPasswordConfirmation: string) {
    this.token = token;
    this.newPassword = newPassword;
    this.newPasswordConfirmation = newPasswordConfirmation;
  }
}
