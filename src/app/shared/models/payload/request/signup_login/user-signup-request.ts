export class UserSignupRequest {

  firstName: string;
  lastName: string;
  pseudo: string;
  email: string;
  password: string;
  getNewsletter: boolean;


  constructor(firstName: string,
              lastName: string,
              pseudo: string,
              email: string,
              password: string,
              getNewsletter: boolean) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.pseudo = pseudo;
    this.email = email;
    this.password = password;
    this.getNewsletter = getNewsletter;
  }
}
