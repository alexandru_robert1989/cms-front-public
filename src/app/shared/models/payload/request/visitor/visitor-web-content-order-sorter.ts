export class VisitorWebContentOrderSorter {


  title: string | undefined;
  id: string | undefined;


  constructor(title: string | undefined,
              id: string | undefined) {
    this.title = title;
    this.id = id;
  }
}
