export class AddCommentRequest {

  parentCommentId: string | number |undefined;

  webContentId: string | number | undefined;

  title: string;

  text: string;


  constructor(parentCommentId: string | number| undefined,
              webContentId: string | number | undefined,
              title: string,
              text: string) {
    this.parentCommentId = parentCommentId;
    this.webContentId = webContentId;
    this.title = title;
    this.text = text;
  }
}
