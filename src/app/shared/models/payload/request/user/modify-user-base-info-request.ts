export class ModifyUserBaseInfoRequest {

  firstName: string;
  lastName: string;
  pseudo: string;
  getNewsletter: boolean;
  email: string | undefined;


  constructor(firstName: string,
              lastName: string,
              pseudo: string,
              getNewsletter: boolean,
              email: string | undefined) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.pseudo = pseudo;
    this.getNewsletter = getNewsletter;
    this.email = email;
  }
}
