export class ModifyEmailAddressRequest {

  password: string;
  oldEmail: string;
  newEmail: string;


  constructor(password: string,
              oldEmail: string,
              newEmail: string) {
    this.password = password;
    this.oldEmail = oldEmail;
    this.newEmail = newEmail;
  }
}
