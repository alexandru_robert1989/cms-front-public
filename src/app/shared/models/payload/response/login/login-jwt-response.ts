export class LoginJwtResponse {

  token: string;
  type: string;
  id: number;
  email: string;
  role: Array<string>;


  constructor(token: string,
              type: string,
              id: number,
              email: string,
              role: Array<string>) {
    this.token = token;
    this.type = type;
    this.id = id;
    this.email = email;
    this.role = role;
  }
}
