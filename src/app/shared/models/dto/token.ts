import {User} from './user';

export class Token {

  id: number;
  token: string;
  expirationDate: Date;
  user: User;


  constructor(id: number,
              token: string,
              expirationDate: Date,
              user: User) {
    this.id = id;
    this.token = token;
    this.expirationDate = expirationDate;
    this.user = user;
  }
}
