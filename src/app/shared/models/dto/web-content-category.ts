import {WebContent} from './web-content';

export class WebContentCategory {


  id: number | undefined;
  versionId: number | undefined;
  inUse: boolean;
  published: boolean;
  title: string;
  description: string;
  creationDate: Date | undefined;
  editionDate: Date | undefined;
  suppressionDate: Date | undefined;
  webContents: Array<WebContent> | undefined;
  totalFoundWebContents: number | undefined;
  totalWebContents: number;
  totalPublishedWebContents: number;
  totalWaitingPublicationWebContents: number;
  totalNotSubmittedForPublicationWebContents: number;
  totalDeletedWebContents: number;

  defaultDeletableContent: boolean;


  constructor(id: number | undefined, versionId: number | undefined, inUse: boolean, published: boolean, title: string, description: string, creationDate: Date | undefined, editionDate: Date | undefined, suppressionDate: Date | undefined, webContents: Array<WebContent> | undefined, totalFoundWebContents: number | undefined, totalWebContents: number, totalPublishedWebContents: number, totalWaitingPublicationWebContents: number, totalNotSubmittedForPublicationWebContents: number, totalDeletedWebContents: number, defaultDeletableContent: boolean) {
    this.id = id;
    this.versionId = versionId;
    this.inUse = inUse;
    this.published = published;
    this.title = title;
    this.description = description;
    this.creationDate = creationDate;
    this.editionDate = editionDate;
    this.suppressionDate = suppressionDate;
    this.webContents = webContents;
    this.totalFoundWebContents = totalFoundWebContents;
    this.totalWebContents = totalWebContents;
    this.totalPublishedWebContents = totalPublishedWebContents;
    this.totalWaitingPublicationWebContents = totalWaitingPublicationWebContents;
    this.totalNotSubmittedForPublicationWebContents = totalNotSubmittedForPublicationWebContents;
    this.totalDeletedWebContents = totalDeletedWebContents;
    this.defaultDeletableContent = defaultDeletableContent;
  }
}
