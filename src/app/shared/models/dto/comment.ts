import {User} from './user';
import {WebContent} from './web-content';

export class Comment {


  id: number;
  title: string;
  text: string;
  creationDate: Date;
  publishingDate: Date;
  suppressionDate: Date;
  user: User;
  parentComment: Comment;
  webContent: WebContent;
  childrenComments: Array<Comment>;

  defaultDeletableContent: boolean;


  constructor(id: number, title: string, text: string, creationDate: Date, publishingDate: Date, suppressionDate: Date, user: User, parentComment: Comment, webContent: WebContent, childrenComments: Array<Comment>, defaultDeletableContent: boolean) {
    this.id = id;
    this.title = title;
    this.text = text;
    this.creationDate = creationDate;
    this.publishingDate = publishingDate;
    this.suppressionDate = suppressionDate;
    this.user = user;
    this.parentComment = parentComment;
    this.webContent = webContent;
    this.childrenComments = childrenComments;
    this.defaultDeletableContent = defaultDeletableContent;
  }
}
