import {User} from './user';


export class Message {

  id: number;
  creationDate: Date;
  suppressionDate: Date;
  subject: string;
  text: string;
  parentMessage: Message;
  sender: User;
  recipients: Array<User>;

  defaultDeletableContent: boolean;


  constructor(id: number, creationDate: Date, suppressionDate: Date, subject: string, text: string, parentMessage: Message, sender: User, recipients: Array<User>, defaultDeletableContent: boolean) {
    this.id = id;
    this.creationDate = creationDate;
    this.suppressionDate = suppressionDate;
    this.subject = subject;
    this.text = text;
    this.parentMessage = parentMessage;
    this.sender = sender;
    this.recipients = recipients;
    this.defaultDeletableContent = defaultDeletableContent;
  }
}
