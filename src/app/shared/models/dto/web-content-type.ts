import {WebContent} from './web-content';

export class WebContentType {


  id: number | undefined;
  versionId: number | undefined;
  inUse: boolean | undefined;
  published: boolean;
  title: string;
  description: string | undefined;
  isNavbarItem: boolean | undefined;
  isFooterItem: boolean | undefined;
  acceptsImages: boolean | undefined;
  hasUniqueImage: boolean | undefined;
  creationDate: Date | undefined;
  editionDate: Date | undefined;
  suppressionDate: Date | undefined;
  hasLimitedModification: boolean | undefined;

  webContents: Array<WebContent> | undefined;
  webContentType: WebContentType | undefined;
  totalFoundWebContents: number | undefined;
  childrenTypes: WebContentType | undefined | any;

  totalWebContents: number;
  totalPublishedWebContents: number;
  totalWaitingPublicationWebContents: number;
  totalNotSubmittedForPublicationWebContents: number;
  totalDeletedWebContents: number;

  defaultDeletableContent: boolean;

  constructor(id: number | undefined, versionId: number | undefined, inUse: boolean | undefined, published: boolean, title: string, description: string | undefined, isNavbarItem: boolean | undefined, isFooterItem: boolean | undefined, acceptsImages: boolean | undefined, hasUniqueImage: boolean | undefined, creationDate: Date | undefined, editionDate: Date | undefined, suppressionDate: Date | undefined, hasLimitedModification: boolean | undefined, webContents: Array<WebContent> | undefined, webContentType: WebContentType | undefined, totalFoundWebContents: number | undefined, childrenTypes: any, totalWebContents: number, totalPublishedWebContents: number, totalWaitingPublicationWebContents: number, totalNotSubmittedForPublicationWebContents: number, totalDeletedWebContents: number, defaultDeletableContent: boolean) {
    this.id = id;
    this.versionId = versionId;
    this.inUse = inUse;
    this.published = published;
    this.title = title;
    this.description = description;
    this.isNavbarItem = isNavbarItem;
    this.isFooterItem = isFooterItem;
    this.acceptsImages = acceptsImages;
    this.hasUniqueImage = hasUniqueImage;
    this.creationDate = creationDate;
    this.editionDate = editionDate;
    this.suppressionDate = suppressionDate;
    this.hasLimitedModification = hasLimitedModification;
    this.webContents = webContents;
    this.webContentType = webContentType;
    this.totalFoundWebContents = totalFoundWebContents;
    this.childrenTypes = childrenTypes;
    this.totalWebContents = totalWebContents;
    this.totalPublishedWebContents = totalPublishedWebContents;
    this.totalWaitingPublicationWebContents = totalWaitingPublicationWebContents;
    this.totalNotSubmittedForPublicationWebContents = totalNotSubmittedForPublicationWebContents;
    this.totalDeletedWebContents = totalDeletedWebContents;
    this.defaultDeletableContent = defaultDeletableContent;
  }
}
