import {User} from './user';
import {WebContentCategory} from './web-content-category';
import {WebContentType} from './web-content-type';
import {Comment} from './comment';

export class WebContent {

  id: number | undefined;
  versionId: number | undefined;
  inUse: boolean | undefined;
  title: string;
  htmlContent: string;
  rawContent: string;
  creationDate: Date | undefined;
  editionDate: Date | undefined;
  publishingDate: Date | undefined;
  suppressionDate: Date | undefined;
  acceptsComments: boolean;
  submittedForPublication: boolean;
  published: boolean;
  anonymousUser: boolean;
  user: User;
  categories: Array<WebContentCategory> | undefined;
  type: WebContentType;
  comments: Array<Comment> | undefined;
  totalComments: number;
  totalPublishedComments: number;
  totalWaitingPublicationComments: number;
  totalDeletedComments: number;

  defaultDeletableContent: boolean;


  constructor(id: number | undefined, versionId: number | undefined, inUse: boolean | undefined, title: string, htmlContent: string, rawContent: string, creationDate: Date | undefined, editionDate: Date | undefined, publishingDate: Date | undefined, suppressionDate: Date | undefined, acceptsComments: boolean, submittedForPublication: boolean, published: boolean, anonymousUser: boolean, user: User, categories: Array<WebContentCategory> | undefined, type: WebContentType, comments: Array<Comment> | undefined, totalComments: number, totalPublishedComments: number, totalWaitingPublicationComments: number, totalDeletedComments: number, defaultDeletableContent: boolean) {
    this.id = id;
    this.versionId = versionId;
    this.inUse = inUse;
    this.title = title;
    this.htmlContent = htmlContent;
    this.rawContent = rawContent;
    this.creationDate = creationDate;
    this.editionDate = editionDate;
    this.publishingDate = publishingDate;
    this.suppressionDate = suppressionDate;
    this.acceptsComments = acceptsComments;
    this.submittedForPublication = submittedForPublication;
    this.published = published;
    this.anonymousUser = anonymousUser;
    this.user = user;
    this.categories = categories;
    this.type = type;
    this.comments = comments;
    this.totalComments = totalComments;
    this.totalPublishedComments = totalPublishedComments;
    this.totalWaitingPublicationComments = totalWaitingPublicationComments;
    this.totalDeletedComments = totalDeletedComments;
    this.defaultDeletableContent = defaultDeletableContent;
  }
}
