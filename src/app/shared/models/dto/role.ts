import {User} from './user';

export class Role {


  id: number;
  versionId: number;
  inUse: boolean;
  title: string;
  description: string;
  users: Array<User>;
  creationDate: Date;
  editionDate: Date;
  suppressionDate: Date;
  totalUsers: number;
  totalNotDeletedUsers: number;
  totalDeletedUsers: number;


  constructor(id: number,
              versionId: number,
              inUse: boolean,
              title: string,
              description: string,
              users: Array<User>,
              creationDate: Date,
              editionDate: Date,
              suppressionDate: Date,
              totalUsers: number,
              totalNotDeletedUsers: number,
              totalDeletedUsers: number) {
    this.id = id;
    this.versionId = versionId;
    this.inUse = inUse;
    this.title = title;
    this.description = description;
    this.users = users;
    this.creationDate = creationDate;
    this.editionDate = editionDate;
    this.suppressionDate = suppressionDate;
    this.totalUsers = totalUsers;
    this.totalNotDeletedUsers = totalNotDeletedUsers;
    this.totalDeletedUsers = totalDeletedUsers;
  }
}
