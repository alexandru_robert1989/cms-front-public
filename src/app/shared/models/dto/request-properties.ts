import {Role} from './role';

export class RequestProperties {


  id: number | undefined;
  title: string;
  role: Role;
  description: string | undefined;
  creationDate: Date | undefined;
  editionDate: Date | undefined;
  suppressionDate: Date | undefined;
  isInUse: boolean;
  onlyPublishedCategories: boolean;
  onlyPublishedSortingCategories: boolean;
  onlyPublishedTypes: boolean;
  onlyPublishedNavbarTypes: boolean;
  onlyPublishedFooterTypes: boolean;
  onlyPublishedSortingTypes: boolean;


  constructor(title: string,
              role: Role,
              isInUse: boolean,
              onlyPublishedCategories: boolean,
              onlyPublishedSortingCategories: boolean,
              onlyPublishedTypes: boolean,
              onlyPublishedNavbarTypes: boolean,
              onlyPublishedFooterTypes: boolean,
              onlyPublishedSortingTypes: boolean,
              id?: number | undefined,
              description?: string | undefined,
              creationDate?: Date | undefined,
              editionDate?: Date | undefined,
              suppressionDate?: Date | undefined) {
    this.id = id;
    this.title = title;
    this.role = role;
    this.description = description;
    this.creationDate = creationDate;
    this.editionDate = editionDate;
    this.suppressionDate = suppressionDate;
    this.isInUse = isInUse;
    this.onlyPublishedCategories = onlyPublishedCategories;
    this.onlyPublishedSortingCategories = onlyPublishedSortingCategories;
    this.onlyPublishedTypes = onlyPublishedTypes;
    this.onlyPublishedNavbarTypes = onlyPublishedNavbarTypes;
    this.onlyPublishedFooterTypes = onlyPublishedFooterTypes;
    this.onlyPublishedSortingTypes = onlyPublishedSortingTypes;
  }
}
