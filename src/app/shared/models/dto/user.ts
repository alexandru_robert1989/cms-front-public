import {WebContent} from './web-content';
import {Comment} from './comment';
import {Token} from './token';
import {Role} from './role';
import {Message} from './message';

export class User {


  id: number | undefined;
  firstName: string | undefined;
  lastName: string | undefined;
  pseudo: string | undefined;
  email: string | undefined;
  password: string |undefined;
  signUpDate: Date | undefined;
  profileImageValidated: boolean | undefined;
  emailValidated: boolean | undefined;
  getNewsletter: boolean | undefined;
  selfDeleted: boolean | undefined;
  suppressionDate: Date | undefined;
  webContents: Array<WebContent> | undefined;
  comments: Array<Comment> | undefined;
  tokens: Array<Token> | undefined;
  roles: Array<Role> | undefined;
  receivedMessages: Array<Message> | undefined;
  sentMessages: Array<Message> | undefined;
  totalfoundWebContents: number | undefined;

  totalWebContents: number;
  totalPublishedWebContents: number;
  totalWaitingPublicationWebContents: number;
  totalNotSubmittedForPublicationWebContents: number;
  totalDeletedWebContents: number;

   totalSentComments: number;
   totalPublishedComments: number;
   totalWaitingPublicationComments: number;
   totalDeletedComments: number;

  defaultDeletableContent: boolean;


  constructor(id: number | undefined, firstName: string | undefined, lastName: string | undefined, pseudo: string | undefined, email: string | undefined, password: string | undefined, signUpDate: Date | undefined, profileImageValidated: boolean | undefined, emailValidated: boolean | undefined, getNewsletter: boolean | undefined, selfDeleted: boolean | undefined, suppressionDate: Date | undefined, webContents: Array<WebContent> | undefined, comments: Array<Comment> | undefined, tokens: Array<Token> | undefined, roles: Array<Role> | undefined, receivedMessages: Array<Message> | undefined, sentMessages: Array<Message> | undefined, totalfoundWebContents: number | undefined, totalWebContents: number, totalPublishedWebContents: number, totalWaitingPublicationWebContents: number, totalNotSubmittedForPublicationWebContents: number, totalDeletedWebContents: number, totalSentComments: number, totalPublishedComments: number, totalWaitingPublicationComments: number, totalDeletedComments: number, defaultDeletableContent: boolean) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.pseudo = pseudo;
    this.email = email;
    this.password = password;
    this.signUpDate = signUpDate;
    this.profileImageValidated = profileImageValidated;
    this.emailValidated = emailValidated;
    this.getNewsletter = getNewsletter;
    this.selfDeleted = selfDeleted;
    this.suppressionDate = suppressionDate;
    this.webContents = webContents;
    this.comments = comments;
    this.tokens = tokens;
    this.roles = roles;
    this.receivedMessages = receivedMessages;
    this.sentMessages = sentMessages;
    this.totalfoundWebContents = totalfoundWebContents;
    this.totalWebContents = totalWebContents;
    this.totalPublishedWebContents = totalPublishedWebContents;
    this.totalWaitingPublicationWebContents = totalWaitingPublicationWebContents;
    this.totalNotSubmittedForPublicationWebContents = totalNotSubmittedForPublicationWebContents;
    this.totalDeletedWebContents = totalDeletedWebContents;
    this.totalSentComments = totalSentComments;
    this.totalPublishedComments = totalPublishedComments;
    this.totalWaitingPublicationComments = totalWaitingPublicationComments;
    this.totalDeletedComments = totalDeletedComments;
    this.defaultDeletableContent = defaultDeletableContent;
  }
}
