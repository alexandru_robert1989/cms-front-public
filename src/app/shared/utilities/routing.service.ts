import {Injectable} from '@angular/core';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RoutingService {

  constructor(private router: Router) {
  }

  static reloadPage(): any {
    window.location.reload();
  }

  goToHomePage(): any {
    this.router.navigate(['']);
  }


  goToLogin(): any {
    this.router.navigate(['connexion']);
  }

  goToSignup(): any {
    this.router.navigate(['inscription']);
  }


  goTo(chemin: string): any {
    this.router.navigate([chemin]);
  }

  goToPageNotFound(): any {
    this.router.navigate(['404']);
  }

}
