import {Injectable} from '@angular/core';
import {Subscription} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UnsubscribeService {

  constructor() {
  }


  static unsubscribe(subscription: Subscription | undefined): any {
    if (subscription) {
      subscription.unsubscribe();
    }
  }

  static unsubscribeAll(subscriptions: Array<Subscription>): any {
    if (subscriptions.length >= 1) {
      subscriptions.forEach(s => {
        s.unsubscribe();
      });
    }
  }

}
