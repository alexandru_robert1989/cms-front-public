import {AbstractControl, ValidationErrors} from '@angular/forms';

export class SignupValidators {


  static firstOrLastNameMaxLength(control: AbstractControl): ValidationErrors | null {
    if ((control.value as string)?.replace(/\s\s+/g, ' ').length > 60) {
      return {
        firstOrLastNameMaxLength: {
          customFirstOrLastNameMaxLength: 60
        }
      };
    }
    return null;
  }

  static firstOrLastNameMinLength(control: AbstractControl): ValidationErrors | null {

    if ((control.value as string)?.trim().length < 2) {
      return {
        firstOrLastNameMinLength: {
          customFirstOrLastNamMinLength: 2
        }
      };
    }
    return null;
  }


  static pseudoMaxLength(control: AbstractControl): ValidationErrors | null {
    if ((control.value as string)?.replace(/\s\s+/g, ' ').length > 50) {
      return {
        pseudoMaxLength: {
          customPseudoMaxLength: 50
        }
      };
    }
    return null;
  }


  static pseudoMinLength(control: AbstractControl): ValidationErrors | null {
    if ((control.value as string)?.replace(/\s\s+/g, ' ').length < 3) {
      return {
        pseudoMinLength: {
          customPseudoMinLength: 3
        }
      };
    }
    return null;
  }


}
