import {Component, OnDestroy} from '@angular/core';
import {Subscription} from 'rxjs';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LoginService} from '../../../../core/services/signup_login/login.service';
import {UnsubscribeService} from '../../../utilities/unsubscribe.service';
import {LoginRequest} from '../../../models/payload/request/signup_login/login-request';
import {LoginJwtResponse} from '../../../models/payload/response/login/login-jwt-response';
import {TokenStorageService} from '../../../../core/services/signup_login/token-storage.service';
import {RoutingService} from '../../../utilities/routing.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnDestroy {

  subscriptions = new Array<Subscription>();

  loginForm: FormGroup;
  authenticationException: boolean | undefined;
  loginUnattendedException: boolean | undefined;
  authentication: LoginJwtResponse | undefined;
  passwordTextInput: boolean | undefined;

  sendingRequest: boolean | undefined;

  passwordResetEmailForm: FormGroup;

  showPasswordResetEmailForm: boolean | undefined;
  passwordResetEmailSuccess: boolean | undefined;
  passwordResetEmailMessageException: boolean | undefined;
  passwordResetEmailInvalidRequest: boolean | undefined;
  passwordResetEmailUnattendedException: boolean | undefined;


  constructor(private loginService: LoginService,
              private formBuilder: FormBuilder,
              private tokenStorageService: TokenStorageService,
              private routingService: RoutingService) {
    const sessionUser = this.tokenStorageService.getUser();
    if (sessionUser) {
      this.routingService.goToHomePage();
    }
    this.loginForm = this.initLoginForm();
    this.passwordResetEmailForm = this.initPasswordResetForm();
  }

  get email(): any {
    return this.loginForm.get('email');
  }

  get password(): any {
    return this.loginForm.get('password');
  }

  get resetEmail(): any {
    return this.passwordResetEmailForm.get('resetEmail');
  }

  onLoginFormSubmit(): any {

    const formValues = this.loginForm.value;
    const request = new LoginRequest(formValues.email, formValues.password);
    this.sendingRequest = true;
    this.subscriptions.push(
      this.loginService.login(request)
        .subscribe(value => {
          this.setLoginRequestOutputsToUndefined();
          this.authentication = value;
          if (this.authentication) {
            this.tokenStorageService.saveToken(this.authentication?.token);
            this.tokenStorageService.saveUser(this.authentication);
            // console.log('received values ', this.authentication);
            setTimeout(() => {
              this.routingService.goToHomePage();
            }, 2000);
          }

        }, error => {
          // console.log('error ', error);
          this.setLoginRequestOutputsToUndefined();
          if (error.error.exceptionMessage === 'authentication_exception') {
            this.authenticationException = true;
            this.loginUnattendedException = undefined;
          } else {
            this.authenticationException = undefined;
            this.loginUnattendedException = true;
          }
        })
    );
  }

  onPasswordResetFormSubmit(): any {
    const formValues = this.passwordResetEmailForm.value;
    this.sendingRequest = true;

    this.subscriptions.push(
      this.loginService.sendPasswordResetEmail(formValues.resetEmail)
        .subscribe(value => {
            this.setPasswordResetRequestOutputsToUndefined();
            this.passwordResetEmailSuccess = true;
            // console.log('success ', value);
          }, error => {
            this.setPasswordResetRequestOutputsToUndefined();
            // console.log('error', error);
            if (error.error.exceptionMessage === 'invalid_request') {
              this.passwordResetEmailInvalidRequest = true;
            } else if (error.error.exceptionMessage === 'messaging_exception') {
              this.passwordResetEmailMessageException = true;
            } else {
              this.passwordResetEmailUnattendedException = true;
            }
          }
        )
    );
  }

  initLoginForm(): FormGroup {
    return this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  initPasswordResetForm(): FormGroup {
    return this.formBuilder.group({
      resetEmail: ['', [Validators.required, Validators.email]]
    });
  }

  setPasswordResetRequestOutputsToUndefined(): void {
    this.sendingRequest = undefined;
    this.passwordResetEmailSuccess = undefined;
    this.passwordResetEmailMessageException = undefined;
    this.passwordResetEmailInvalidRequest = undefined;
    this.passwordResetEmailUnattendedException = undefined;
  }

  setLoginRequestOutputsToUndefined(): void {
    this.sendingRequest = undefined;
    this.authenticationException = undefined;
    this.loginUnattendedException = undefined;
    this.authentication = undefined;
  }


  togglePasswordResetEmailDisplay(): void {
    this.showPasswordResetEmailForm = !this.showPasswordResetEmailForm;
  }

  togglePasswordTextInput(): void {
    this.passwordTextInput = !this.passwordTextInput;
  }

  ngOnDestroy(): void {
    UnsubscribeService.unsubscribeAll(this.subscriptions);
  }

}
