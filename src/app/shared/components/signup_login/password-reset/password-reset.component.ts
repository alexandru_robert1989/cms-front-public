import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TokenStorageService} from '../../../../core/services/signup_login/token-storage.service';
import {RoutingService} from '../../../utilities/routing.service';
import {UnsubscribeService} from '../../../utilities/unsubscribe.service';
import {PasswordResetRequest} from '../../../models/payload/request/signup_login/password-reset-request';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {LoginService} from '../../../../core/services/signup_login/login.service';

@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.css']
})
export class PasswordResetComponent implements OnDestroy {

  subscriptions = new Array<Subscription>();
  form: FormGroup;
  passwordTextInput: boolean | undefined;
  passwordConfirmationTextInput: boolean | undefined;

  token: string | undefined;


  successMessage: boolean | undefined;

  passwordMismatch: boolean | undefined;
  invalidRequest: boolean | undefined;
  missingToken: boolean | undefined;
  invalidToken: boolean | undefined;
  unattendedException: boolean | undefined;


  sendingRequest: boolean | undefined;


  passwordResetEmailForm: FormGroup;
  showPasswordResetEmailForm: boolean | undefined;
  passwordResetEmailSuccess: boolean | undefined;
  passwordResetEmailMessageException: boolean | undefined;
  passwordResetEmailInvalidRequest: boolean | undefined;
  passwordResetEmailUnattendedException: boolean | undefined;


  constructor(private loginService: LoginService,
              private formBuilder: FormBuilder,
              private tokenStorageService: TokenStorageService,
              private routingService: RoutingService,
              private router: Router,
              route: ActivatedRoute) {

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.token = route.snapshot.params.token;
        console.log('token ', this.token);
        if (!this.token) {
          this.missingToken = true;
        }
      }
    });


    this.form = this.formBuilder.group({
      password: ['', [Validators.required,
        Validators.minLength(8),
        Validators.maxLength(20)]],

      passwordConfirmation: ['', [Validators.required,
        Validators.minLength(8),
        Validators.maxLength(20)]]

    });

    this.passwordResetEmailForm = this.formBuilder.group({
      resetEmail: ['', [Validators.required, Validators.email]]
    });




  }

  get password(): any {
    return this.form.get('password');
  }

  get passwordConfirmation(): any {
    return this.form.get('passwordConfirmation');
  }

  get resetEmail(): any {
    return this.passwordResetEmailForm.get('resetEmail');
  }



  togglePasswordTextInput(): void {
    this.passwordTextInput = !this.passwordTextInput;
  }

  togglePasswordConfirmationTextInput(): void {
    this.passwordConfirmationTextInput = !this.passwordConfirmationTextInput;
  }


  checkPasswordMatching(): any {
    if (this.passwordMismatch) {
      const formValues = this.form.value;
      if (formValues.password === formValues.passwordConfirmation) {
        this.passwordMismatch = undefined;
      }
    }
  }


  onFormSubmit(): any {
    const formValues = this.form.value;
    this.sendingRequest = true;
    this.successMessage = undefined;
    this.invalidRequest = undefined;
    this.invalidToken = undefined;

    this.passwordMismatch = formValues.password !== formValues.passwordConfirmation;


    if (this.passwordMismatch) {
      this.sendingRequest = undefined;
      return;
    }

    const request = new PasswordResetRequest(this.token, formValues.password, formValues.passwordConfirmation);
    console.log('request ', request);

    this.subscriptions.push(this.loginService.resetPassword(request).subscribe(value => {
      console.log('result ', value);
      this.successMessage = true;
      this.sendingRequest = undefined;
      this.passwordMismatch = undefined;
      this.invalidToken = undefined;
      this.missingToken = undefined;
      this.invalidRequest = undefined;
      this.unattendedException = undefined;
    }, error => {
      this.sendingRequest = undefined;
      this.passwordMismatch = undefined;
      this.invalidToken = undefined;
      this.missingToken = undefined;
      this.invalidRequest = undefined;
      this.unattendedException = undefined;
      if (error.error.exceptionMessage === 'password_mismatch') {
        this.passwordMismatch = true;
      } else if (error.error.exceptionMessage === 'invalid_or_expired_token') {
        this.invalidToken = true;
      } else if (error.error.exceptionMessage === 'invalid_request') {
        this.invalidRequest = true;
      } else if (error.error.exceptionMessage === 'missing_token') {
        this.missingToken = true;
      } else {
        this.unattendedException = true;
      }
    }));


  }


  onPasswordResetFormSubmit(): any {
    const formValues = this.passwordResetEmailForm.value;
    this.sendingRequest = true;
    this.passwordResetEmailSuccess = undefined;
    this.passwordResetEmailMessageException = undefined;
    this.passwordResetEmailInvalidRequest = undefined;
    this.passwordResetEmailUnattendedException = undefined;

    this.subscriptions.push(
      this.loginService.sendPasswordResetEmail(formValues.resetEmail)
        .subscribe(value => {
            this.sendingRequest = undefined;
            this.passwordResetEmailMessageException = undefined;
            this.passwordResetEmailInvalidRequest = undefined;
            this.passwordResetEmailUnattendedException = undefined;
            this.passwordResetEmailSuccess = true;
            // console.log('success ', value);
          }, error => {
            this.sendingRequest = undefined;
            this.passwordResetEmailSuccess = undefined;
            this.passwordResetEmailMessageException = undefined;
            this.passwordResetEmailInvalidRequest = undefined;
            this.passwordResetEmailUnattendedException = undefined;
            // console.log('error', error);
            if (error.error.exceptionMessage === 'invalid_request') {
              this.passwordResetEmailInvalidRequest = true;
            } else if (error.error.exceptionMessage === 'messaging_exception') {
              this.passwordResetEmailMessageException = true;
            } else {
              this.passwordResetEmailUnattendedException = true;
            }
          }
        )
    );
  }

  ngOnDestroy(): void {
    UnsubscribeService.unsubscribeAll(this.subscriptions);
  }


}
