import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {SignupService} from '../../../../core/services/signup_login/signup.service';
import {UnsubscribeService} from '../../../utilities/unsubscribe.service';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-email-validation',
  templateUrl: './email-validation.component.html',
  styleUrls: ['./email-validation.component.css']
})
export class EmailValidationComponent implements OnDestroy {

  subscriptions = new Array<Subscription>();
  success: boolean | undefined;
  invalidToken: boolean | undefined;


  constructor(private signupService: SignupService,
              private router: Router,
              private route: ActivatedRoute) {

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        const token = this.route.snapshot.params.token;

        if (token !== undefined) {
          this.subscriptions.push(
            this.signupService.emailValidation(token)
              .subscribe(success => {
                  this.invalidToken = undefined;
                  this.success = true;
                }, error => {
                  this.success = undefined;
                  this.invalidToken = true;
                }
              )
          );
        }else {
          this.success = undefined;
          this.invalidToken = true;
        }

      }
    });

  }

  ngOnDestroy(): void {
    UnsubscribeService.unsubscribeAll(this.subscriptions);
  }


}
