import {Component, OnDestroy} from '@angular/core';
import {SignupService} from '../../../../core/services/signup_login/signup.service';
import {Subscription} from 'rxjs';
import {UnsubscribeService} from '../../../utilities/unsubscribe.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SignupValidators} from '../../../utilities/validators/signup_validators';
import {UserSignupRequest} from '../../../models/payload/request/signup_login/user-signup-request';
import {TokenStorageService} from '../../../../core/services/signup_login/token-storage.service';
import {RoutingService} from '../../../utilities/routing.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnDestroy {

  subscriptions = new Array<Subscription>();
  form: FormGroup;
  passwordMismatch: boolean | undefined;
  passwordTextInput: boolean | undefined;
  passwordConfirmationTextInput: boolean | undefined;
  success: boolean | undefined;
  unattendedException: boolean | undefined;
  mailException: boolean | undefined;
  invalidRequest: boolean | undefined;
  unavailablePseudo: boolean | undefined;
  sendingRequest: boolean | undefined;

  invalidPseudos = new Array<string>();


  constructor(private signupService: SignupService,
              private formBuilder: FormBuilder,
              private tokenStorageService: TokenStorageService,
              private routingService: RoutingService) {

    const sessionUser = this.tokenStorageService.getUser();
    if (sessionUser) {
      this.routingService.goToHomePage();
    }
    this.form = this.initForm();

  }


  get lastName(): any {
    return this.form.get('lastName');
  }

  get firstName(): any {
    return this.form.get('firstName');
  }

  get pseudo(): any {
    return this.form.get('pseudo');
  }

  get email(): any {
    return this.form.get('email');
  }

  get password(): any {
    return this.form.get('password');
  }

  get passwordConfirmation(): any {
    return this.form.get('passwordConfirmation');
  }

  get getNewsletter(): any {
    return this.form.get('getNewsletter');
  }

  get legalMentions(): any {
    return this.form.get('legalMentions');
  }

  onFormSubmit(): any {
    const formValues = this.form.value;

    this.setRequestOutputsToUndefined();
    this.sendingRequest = true;
    this.passwordMismatch = formValues.password !== formValues.passwordConfirmation;
    if (this.passwordMismatch) {
      this.sendingRequest = undefined;
      return;
    }

    if ((formValues.pseudo as string).trim() === 'anonyme') {
      this.unavailablePseudo = true;
      this.invalidPseudos.push(formValues.pseudo);
      this.sendingRequest = undefined;
      return;
    }


    const request = new UserSignupRequest(formValues.firstName,
      formValues.lastName, formValues.pseudo, formValues.email, formValues.password, formValues.getNewsletter);
    // console.log('request ', request);

    this.subscriptions.push(this.signupService.signup(request).subscribe(value => {
      console.log('result ', value);
      this.success = true;
      this.sendingRequest = undefined;
    }, error => {
      this.setRequestOutputsToUndefined();
      if (error.error.exceptionMessage === 'unavailable_pseudo') {
        this.unavailablePseudo = true;
        this.invalidPseudos.push(formValues.pseudo);
      } else if (error.error.exceptionMessage === 'unattended_exception') {
        this.unattendedException = true;
      } else if (error.error.exceptionMessage === 'invalid_request') {
        this.invalidRequest = true;
      } else if (error.error.exceptionMessage === 'mail_exception') {
        this.mailException = true;
      } else {
        this.unattendedException = true;
      }
    }));


  }


  initForm(): FormGroup {
    return this.formBuilder.group({

      lastName: ['', [Validators.required,
        SignupValidators.firstOrLastNameMaxLength,
        SignupValidators.firstOrLastNameMinLength,
        Validators.pattern(`^(^[\\s]*[A-Za-zÀ-ÿ]{1,20}([\\s]*[( '-][\\s]*[A-Za-zÀ-ÿ]{1,20}){0,6}[\\s]*)`)]],

      firstName: ['', [Validators.required,
        SignupValidators.firstOrLastNameMaxLength,
        SignupValidators.firstOrLastNameMinLength,
        Validators.pattern(`^(^[\\s]*[A-Za-zÀ-ÿ]{1,20}([\\s]*[( '-][\\s]*[A-Za-zÀ-ÿ]{1,20}){0,6}[\\s]*)`)]],

      pseudo: ['', [Validators.required,
        SignupValidators.pseudoMaxLength,
        SignupValidators.pseudoMinLength,
        Validators.pattern(`^(^[\\s]*([a-zA-Z]){1,}([-_.]?[a-zA-Z\\d]*)?[\\s]*){1,2}[\\s]*$`)]],

      email: ['', [Validators.required,
        Validators.email]],

      password: ['', [Validators.required,
        Validators.minLength(8),
        Validators.maxLength(20)]],

      passwordConfirmation: ['', [Validators.required,
        Validators.minLength(8),
        Validators.maxLength(20)]],

      getNewsletter: [true, [Validators.required]],

      legalMentions: ['', [Validators.requiredTrue]]
    });
  }


  togglePasswordTextInput(): void {
    this.passwordTextInput = !this.passwordTextInput;
  }

  togglePasswordConfirmationTextInput(): void {
    this.passwordConfirmationTextInput = !this.passwordConfirmationTextInput;
  }

  checkPasswordMatching(): any {
    if (this.passwordMismatch) {
      const formValues = this.form.value;
      if (formValues.password === formValues.passwordConfirmation) {
        this.passwordMismatch = undefined;
      }
    }
  }

  actualisePseudoAvailability(): void {
    if (this.unavailablePseudo && !this.invalidPseudos.includes(this.pseudo.value)) {
      this.unavailablePseudo = false;
    } else if (!this.unavailablePseudo && this.invalidPseudos.includes(this.pseudo.value)) {
      this.unavailablePseudo = true;
    }
  }

  setRequestOutputsToUndefined(): void {
    this.success = undefined;
    this.sendingRequest = undefined;
    this.unavailablePseudo = undefined;
    this.unattendedException = undefined;
    this.invalidRequest = undefined;
    this.mailException = undefined;
  }


  ngOnDestroy(): void {
    UnsubscribeService.unsubscribeAll(this.subscriptions);
  }

}
