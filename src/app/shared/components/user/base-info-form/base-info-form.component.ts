import {Component} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SignupValidators} from '../../../utilities/validators/signup_validators';
import {User} from '../../../models/dto/user';
import {UserService} from '../../../../core/services/user/user.service';
import {Subscription} from 'rxjs';
import {ModifyUserBaseInfoRequest} from '../../../models/payload/request/user/modify-user-base-info-request';
import {TokenStorageService} from '../../../../core/services/signup_login/token-storage.service';

@Component({
  selector: 'app-base-info-form',
  templateUrl: './base-info-form.component.html',
  styleUrls: ['./base-info-form.component.css']
})
export class BaseInfoFormComponent {

  subscriptions = new Array<Subscription>();
  invalidPseudos = new Array<string>();

  user: User | undefined;
  form: FormGroup;

  success: boolean | undefined;
  unavailablePseudo: boolean | undefined;
  unauthorised: boolean | undefined;
  unattendedException: boolean | undefined;
  sendingRequest: boolean | undefined;
  unchangedForm: boolean | undefined;

  constructor(private formBuilder: FormBuilder,
              private userService: UserService,
              private tokenService: TokenStorageService) {

    this.form = this.initForm();

    this.subscriptions.push(
      this.userService.getLoggedInUser()
        .subscribe(value => {
          this.user = value;
          this.form = this.initForm();
          this.actualiseFormValid();
        })
    );
  }

  get pseudo(): any {
    return this.form.get('pseudo');
  }

  get lastName(): any {
    return this.form.get('lastName');
  }

  get firstName(): any {
    return this.form.get('firstName');
  }

  get getNewsletter(): any {
    return this.form.get('getNewsletter');
  }

  initForm(): FormGroup {
    return this.formBuilder.group({

      pseudo: [this.user?.pseudo, [Validators.required,
        SignupValidators.pseudoMaxLength,
        SignupValidators.pseudoMinLength,
        Validators.pattern(`^(^[\\s]*([a-zA-Z]){1,}([-_.]?[a-zA-Z\\d]*)?[\\s]*){1,2}[\\s]*$`)]],

      lastName: [this.user?.lastName, [Validators.required,
        SignupValidators.firstOrLastNameMaxLength,
        SignupValidators.firstOrLastNameMinLength,
        Validators.pattern(`^(^[\\s]*[A-Za-zÀ-ÿ]{1,20}([\\s]*[( '-][\\s]*[A-Za-zÀ-ÿ]{1,20}){0,6}[\\s]*)`)]],

      firstName: [this.user?.firstName, [Validators.required,
        SignupValidators.firstOrLastNameMaxLength,
        SignupValidators.firstOrLastNameMinLength,
        Validators.pattern(`^(^[\\s]*[A-Za-zÀ-ÿ]{1,20}([\\s]*[( '-][\\s]*[A-Za-zÀ-ÿ]{1,20}){0,6}[\\s]*)`)]],

      getNewsletter: [this?.user?.getNewsletter, [Validators.required]]

    });
  }


  onFormSubmit(): void {
    const formValues = this.form.value;
    const request = new ModifyUserBaseInfoRequest
    (formValues.firstName, formValues.lastName, formValues.pseudo, formValues.getNewsletter, this.user?.email);

    this.sendingRequest = true;

    this.subscriptions.push(
      this.userService.modifyBaseInfo(request)
        .subscribe(value => {
          this.setRequestOutputsToUndefined();
          this.success = true;
          setTimeout(() => location.reload(), 1500);

        }, error => {
          this.setRequestOutputsToUndefined();
          if (error.error.exceptionMessage === 'unavailable_pseudo') {
            this.unavailablePseudo = true;
            this.invalidPseudos.push(formValues.pseudo);
          } else if (error.error.exceptionMessage === 'unauthorised') {
            this.unauthorised = true;
          } else {
            this.unattendedException = true;
          }
        })
    );


  }

  setRequestOutputsToUndefined(): void {
    this.success = undefined;
    this.sendingRequest = undefined;
    this.unavailablePseudo = undefined;
    this.unauthorised = undefined;
    this.unattendedException = undefined;

  }

  actualisePseudoAvailability(): void {
    this.actualiseFormValid();
    if (this.unavailablePseudo && !this.invalidPseudos.includes(this.pseudo.value)) {
      this.unavailablePseudo = false;
    } else if (!this.unavailablePseudo && this.invalidPseudos.includes(this.pseudo.value)) {
      this.unavailablePseudo = true;
    }
  }

  actualiseFormValid(): void {
    const formValues = this.form.value;

    if (formValues.pseudo === this.user?.pseudo && formValues.firstName === this.user?.firstName
      && formValues.lastName === this.user?.lastName &&
      formValues.getNewsletter === this.user?.getNewsletter) {
      this.unchangedForm = true;
    } else {
      this.unchangedForm = undefined;
    }
  }


}
