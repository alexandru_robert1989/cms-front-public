import {Component, OnDestroy} from '@angular/core';
import {Subscription} from 'rxjs';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TokenStorageService} from '../../../../core/services/signup_login/token-storage.service';
import {UnsubscribeService} from '../../../utilities/unsubscribe.service';
import {ModifyEmailAddressRequest} from '../../../models/payload/request/user/modify-email-address-request';
import {UserService} from '../../../../core/services/user/user.service';
import {VisitorRoutingService} from '../../../../core/services/utilities/visitor/visitor-routing.service';

@Component({
  selector: 'app-modify-email-form',
  templateUrl: './modify-email-form.component.html',
  styleUrls: ['./modify-email-form.component.css']
})
export class ModifyEmailFormComponent implements OnDestroy {

  subscriptions = new Array<Subscription>();
  userEmailAddress: string;

  passwordTextInput: boolean | undefined;

  emailMismatch: boolean | undefined;
  sameEmail: boolean | undefined;

  form: FormGroup;

  sendingRequest: boolean | undefined;
  success: boolean | undefined;
  unauthorised: boolean | undefined;
  incorrectPassword: boolean | undefined;
  unattendedException: boolean | undefined;
  emailNotAvailable: boolean | undefined;
  emailAlreadyOwned: boolean | undefined;
  sendingMailException: boolean | undefined;

  constructor(private tokenService: TokenStorageService,
              private formBuilder: FormBuilder,
              private userService: UserService,
              private routing: VisitorRoutingService) {

    this.userEmailAddress = this.tokenService.getUser()?.email;
    this.form = this.initForm();
  }

  initForm(): FormGroup {

    return this.formBuilder.group({
      password: ['', [Validators.required,
        Validators.minLength(8),
        Validators.maxLength(20)]],

      newEmail: ['', [Validators.required,
        Validators.email]],

      newEmailConfirmation: ['', [Validators.required,
        Validators.email]],


    });
  }

  get password(): any {
    return this.form.get('password');
  }

  get newEmail(): any {
    return this.form.get('newEmail');
  }

  get newEmailConfirmation(): any {
    return this.form.get('newEmailConfirmation');
  }

  onFormSubmit(): void {
    const formValues = this.form.value;
    const newEmail = formValues.newEmail;
    const newEmailConfirmation = formValues.newEmailConfirmation;
    const password = formValues.password;

    if (newEmail !== newEmailConfirmation) {
      this.emailMismatch = true;
      return;
    } else {
      this.emailMismatch = undefined;
    }
    if (newEmail === this.userEmailAddress) {
      this.sameEmail = true;
      return;
    } else {
      this.sameEmail = undefined;
    }

    const request = new ModifyEmailAddressRequest(password, this.userEmailAddress, newEmail);

    this.sendingRequest = true;

    this.subscriptions.push(
      this.userService.modifyEmailAddress(request)
        .subscribe(value => {
          this.setRequestOutputsToUndefined();
          this.success = true;

          setTimeout(() => {
            window.sessionStorage.clear();
            this.routing.goTo('');
          }, 5000);
        }, error => {
          this.setRequestOutputsToUndefined();

          if (error.error.exceptionMessage === 'unauthorised') {
            this.unauthorised = true;
          } else if (error.error.exceptionMessage === 'incorrect_password') {
            this.incorrectPassword = true;
          } else if (error.error.exceptionMessage === 'email_already_owned') {
            this.emailAlreadyOwned = true;
          } else if (error.error.exceptionMessage === 'email_not_available') {
            this.emailNotAvailable = true;
          } else if (error.error.exceptionMessage === 'mail_exception') {
            this.sendingMailException = true;
          } else {
            this.unattendedException = true;
          }
        })
    );


  }


  updateFormValid(): void {

    const formValues = this.form.value;
    const newEmail = formValues.newEmail;
    const newEmailConfirmation = formValues.newEmailConfirmation;

    if (this.emailMismatch) {
      if (newEmail === newEmailConfirmation) {
        this.emailMismatch = undefined;
      }
    }

    if (this.sameEmail) {
      if (newEmail !== this.userEmailAddress) {
        this.sameEmail = undefined;
      }
    }
  }

  togglePasswordTextInput(): void {
    this.passwordTextInput = !this.passwordTextInput;
  }

  setRequestOutputsToUndefined(): void {
    this.success = undefined;
    this.sendingRequest = undefined;
    this.unauthorised = undefined;
    this.incorrectPassword = undefined;
    this.emailAlreadyOwned = undefined;
    this.emailNotAvailable = undefined;
    this.unattendedException = undefined;
    this.sendingMailException = undefined;
  }


  ngOnDestroy(): void {
    UnsubscribeService.unsubscribeAll(this.subscriptions);
  }


}
