import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyEmailFormComponent } from './modify-email-form.component';

describe('ModifyEmailFormComponent', () => {
  let component: ModifyEmailFormComponent;
  let fixture: ComponentFixture<ModifyEmailFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModifyEmailFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyEmailFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
