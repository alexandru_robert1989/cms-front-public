import {Component, OnDestroy} from '@angular/core';
import {UserService} from '../../../../core/services/user/user.service';
import {Subscription} from 'rxjs';
import {UnsubscribeService} from '../../../utilities/unsubscribe.service';
import {NavigationEnd, Router} from '@angular/router';
import {User} from '../../../models/dto/user';

@Component({
  selector: 'app-user-account',
  templateUrl: './user-account.component.html',
  styleUrls: ['./user-account.component.css']
})
export class UserAccountComponent implements OnDestroy {

  subscriptions = new Array<Subscription>();
  user: User | undefined;
  modifyBaseInfo: boolean | undefined;
  modifyEmailAddress: boolean | undefined;

  constructor(private userService: UserService,
              private router: Router) {


    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.subscriptions.push(
          this.userService.getLoggedInUser()
            .subscribe(value => {
              this.user = value;

              console.log('user logged in ', this.user);
            }, error1 => {
              console.log('user error ', error1);
            })
        );
      }
    });
  }


  displayModifyBaseInfo(): void {
    this.modifyBaseInfo = !this.modifyBaseInfo;
    const element = document.getElementById('modifyBaseInfo');
    if (element) {
      element.classList.toggle('btn-primary');
      element.classList.toggle('btn-danger');
    }

  }


  displayModifyEmail(): void {
    this.modifyEmailAddress = !this.modifyEmailAddress;
    const element = document.getElementById('modifyEmail');
    if (element) {
      element.classList.toggle('btn-primary');
      element.classList.toggle('btn-danger');
    }

  }


  ngOnDestroy(): void {
    UnsubscribeService.unsubscribeAll(this.subscriptions);
  }

}
