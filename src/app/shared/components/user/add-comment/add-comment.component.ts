import {Component, Input, OnDestroy} from '@angular/core';
import {CommentService} from '../../../../core/services/user/comment.service';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import {UnsubscribeService} from '../../../utilities/unsubscribe.service';
import {UserService} from '../../../../core/services/user/user.service';
import {RoutingService} from '../../../utilities/routing.service';
import {AddCommentRequest} from '../../../models/payload/request/user/add-comment-request';

@Component({
  selector: 'app-add-comment',
  templateUrl: './add-comment.component.html',
  styleUrls: ['./add-comment.component.css']
})
export class AddCommentComponent implements OnDestroy {

  subscriptions = new Array<Subscription>();
  webContentId: string;
  form: FormGroup;

  routing: RoutingService;

  loggedIn: boolean | undefined;
  confirmedAccount: boolean | undefined;
  dataReceived: boolean | undefined;

  displayedCommentId: number | undefined;

  sendingRequest: boolean | undefined;
  success: boolean | undefined;
  authenticationException: boolean | undefined;
  notFoundException: boolean | undefined;
  invalidRequest: boolean | undefined;
  unattendedException: boolean | undefined;

  commentTitle: string | undefined;
  commentText: string | undefined;


  @Input()
  parentCommentId: number | string | undefined;

  @Input()
  componentId: number | undefined;


  constructor(private commentService: CommentService,
              private route: ActivatedRoute,
              private router: Router,
              private formBuilder: FormBuilder,
              private userService: UserService,
              private routingService: RoutingService) {
    this.routing = this.routingService;
    this.webContentId = this.route.snapshot.params.id;

    this.form = this.initForm();

    this.subscriptions.push(
      this.userService.getLoggedInUser()
        .subscribe(user => {
          this.loggedIn = true;
          //    console.log('user ', user);
          if (user?.emailValidated) {
            this.confirmedAccount = true;
            this.dataReceived = true;
          }
        }, error => {
          this.dataReceived = true;
          this.loggedIn = undefined;
          this.confirmedAccount = undefined;
        })
    );


    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.webContentId = this.route.snapshot.params.id;
      }
    });

  }


  initForm(): FormGroup {
    return this.formBuilder.group({
      title: ['', [Validators.required,
        Validators.minLength(2),
        Validators.maxLength(50)]],
      text: ['', [Validators.required,
        Validators.minLength(2),
        Validators.maxLength(1000)]]
    });
  }


  get title(): any {
    return this.form.get('title');
  }


  get text(): any {
    return this.form.get('text');
  }


  onFormSubmit(): void {
    const formValues = this.form.value;
    const title = formValues.title;
    const text = formValues.text;

    if (this.parentCommentId === -99) {
      this.parentCommentId = undefined;
    }

    const request = new AddCommentRequest(this.parentCommentId, this.webContentId, title, text);

    this.subscriptions.push(
      this.commentService.addComment(request)
        .subscribe(success => {
          this.setLoginRequestOutputsToUndefined();
          if (this.parentCommentId === undefined) {
            this.parentCommentId = -99;
          }
          this.commentTitle = title;
          this.commentText = text;

          this.success = true;
        }, error => {
          this.setLoginRequestOutputsToUndefined();
          if (error.error.exceptionMessage === 'authentication_exception') {
            this.authenticationException = true;
          } else if (error.error.exceptionMessage === 'not_found') {
            this.notFoundException = true;
          } else if (error.error.exceptionMessage === 'invalid_request') {
            this.invalidRequest = true;
          } else {
            this.unattendedException = true;
          }
        })
    );


  }


  displayCommentForm(event: any): void {
    const id = event.target.id;
    if (id === this.displayedCommentId) {
      this.displayedCommentId = undefined;
    } else {
      this.displayedCommentId = event.target.id;
    }
  }


  setLoginRequestOutputsToUndefined(): void {
    this.success = undefined;
    this.sendingRequest = undefined;
    this.authenticationException = undefined;
    this.notFoundException = undefined;
    this.invalidRequest = undefined;
    this.unattendedException = undefined;
  }


  ngOnDestroy(): void {
    UnsubscribeService.unsubscribeAll(this.subscriptions);
  }

}
