import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {UserService} from '../../../../core/services/user/user.service';

@Component({
  selector: 'app-resend-validation-email',
  templateUrl: './resend-validation-email.component.html',
  styleUrls: ['./resend-validation-email.component.css']
})
export class ResendValidationEmailComponent implements OnDestroy {

  subscriptions = new Array<Subscription>();


  sendingNewValidationMailRequest: boolean | undefined;
  newValidationMailSent: boolean | undefined;
  newValidationMailMailException: boolean | undefined;
  newValidationMailUnauthorised: boolean | undefined;
  newValidationMailUnattendedException: boolean | undefined;

  constructor(private userService: UserService) {
  }


  resendValidationMail(): void {
    if (this.sendingNewValidationMailRequest
    ) {
      return;
    }
    this.sendingNewValidationMailRequest = true;
    this.newValidationMailSent = undefined;
    this.newValidationMailMailException = undefined;
    this.newValidationMailUnattendedException = undefined;
    this.newValidationMailUnauthorised = undefined;

    this.subscriptions.push(
      this.userService.resendValidationMail()
        .subscribe(value => {
          this.sendingNewValidationMailRequest = undefined;
          this.newValidationMailSent = true;
        }, error => {
          console.log('error  ', error);
          this.sendingNewValidationMailRequest = undefined;

          if (error.error.exceptionMessage === 'unauthorised') {
            this.newValidationMailUnauthorised = true;
          } else if (error.error.exceptionMessage === 'mail_exception') {
            this.newValidationMailMailException = true;
          } else {
            this.newValidationMailUnattendedException = true;
          }


        })
    );
  }

  ngOnDestroy(): void {
  }

}
