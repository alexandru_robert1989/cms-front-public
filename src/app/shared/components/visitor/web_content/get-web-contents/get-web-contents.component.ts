import {Component, OnDestroy} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {VisitorWebContentService} from '../../../../../core/services/visitor/visitor-web-content.service';
import {Subscription} from 'rxjs';
import {VisitorRoutingService} from '../../../../../core/services/utilities/visitor/visitor-routing.service';
import {WebContent} from '../../../../models/dto/web-content';
import {VisitorSortingItemsService} from '../../../../../core/services/visitor/visitor-sorting-items.service';

import {VisitorWebContentOrderSorter} from '../../../../models/payload/request/visitor/visitor-web-content-order-sorter';
import {VisitorWebContentTypeService} from '../../../../../core/services/visitor/visitor-web-content-type.service';
import {VisitorWebContentCategoryService} from '../../../../../core/services/visitor/visitor-web-content-category.service';
import {VisitorUserService} from '../../../../../core/services/visitor/visitor-user.service';
import {UnsubscribeService} from '../../../../utilities/unsubscribe.service';

@Component({
  selector: 'app-get-web-contents',
  templateUrl: './get-web-contents.component.html',
  styleUrls: ['./get-web-contents.component.css']
})
export class GetWebContentsComponent implements OnDestroy {

  subscriptions = new Array<Subscription>();

  webContents: Array<WebContent> | undefined;
  orderSorters: Array<VisitorWebContentOrderSorter> | undefined;

  routing: VisitorRoutingService;

  urlTypeTitle: string | undefined;
  urlCategoryTitle: string | undefined;
  userPseudo: string | undefined;
  urlSortId: string | undefined;
  urlSort: VisitorWebContentOrderSorter | undefined;

  detailsInnerHtml = 'Afficher détails';

  constructor(private router: Router,
              private route: ActivatedRoute,
              private visitorWebContentService: VisitorWebContentService,
              private visitorSortingService: VisitorSortingItemsService,
              private visitorTypeService: VisitorWebContentTypeService,
              private visitorCategoryService: VisitorWebContentCategoryService,
              private visitorUserService: VisitorUserService,
              private routingService: VisitorRoutingService) {

    this.routing = this.routingService;

    router.events.subscribe(r => {

      if (r instanceof NavigationEnd) {

        // this.setPropertiesOnRouting();

        const urlTypeId = parseInt(this.route.snapshot.params?.typeId, 10);
        this.urlTypeTitle = this.route.snapshot.params?.typeTitle;
        const urlCategoryId = parseInt(this.route.snapshot.params?.catId, 10);
        this.urlCategoryTitle = this.route.snapshot.params?.catTitle;
        const urlUserId = parseInt(this.route.snapshot.params?.userId, 10);
        this.userPseudo = this.route.snapshot.params?.userTitle;
        this.urlSortId = this.route.snapshot.params?.sort;


        this.subscriptions.push(this.visitorSortingService.getOrderSortersItems()
          .subscribe(value => {
            this.orderSorters = value;
            // tslint:disable-next-line:triple-equals
            this.urlSort = this.orderSorters?.find(s => s?.id == this.urlSortId);
          }));


        const t0 = performance.now();

        // @ts-ignore
        this.subscriptions.push(this.visitorWebContentService
          .getWebContentsFromCompleteRequest(urlTypeId, urlCategoryId, urlUserId, this.urlSortId)
          .subscribe(value => {

            // console.log('typeId ', urlTypeId, ' catId ', urlCategoryId, ' userId ', urlUserId, ' sortId ', this.urlSortId);
            const t1 = performance.now();
            this.webContents = value;
            console.log(' Retrieving', this.webContents?.length, ' web contents took ' + (t1 - t0).toFixed(0) + ' milliseconds.');
            //  console.log('web contents ', value);
          }));
      }
    });
  }


  setPropertiesOnRouting(): any {
    this.urlSort = undefined;
    this.urlTypeTitle = undefined;
    this.urlCategoryTitle = undefined;
    this.userPseudo = undefined;
    this.urlSortId = undefined;
  }


  toggleWebContentDetails(event: any): any {

    console.log('id', event.target.id);
    console.log('id', event.target.classList);
    console.log('id', event.target);


    if (event.target.innerHTML === 'Afficher détails') {
      event.target.innerHTML = 'Masquer détails';
    } else {
      event.target.innerHTML = 'Afficher détails';
    }

  }


  ngOnDestroy(): void {
    UnsubscribeService.unsubscribeAll(this.subscriptions);
  }


}
