import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GetWebContentsComponent } from './get-web-contents.component';

describe('GetWebContentsComponent', () => {
  let component: GetWebContentsComponent;
  let fixture: ComponentFixture<GetWebContentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GetWebContentsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GetWebContentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
