import {Component, OnDestroy} from '@angular/core';
import {Subscription} from 'rxjs';
import {VisitorWebContentService} from '../../../../../core/services/visitor/visitor-web-content.service';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {RoutingService} from '../../../../utilities/routing.service';
import {WebContent} from '../../../../models/dto/web-content';
import {VisitorCommentService} from '../../../../../core/services/visitor/visitor-comment.service';
import {UnsubscribeService} from '../../../../utilities/unsubscribe.service';
import {VisitorRoutingService} from '../../../../../core/services/utilities/visitor/visitor-routing.service';

@Component({
  selector: 'app-web-content-by-id',
  templateUrl: './web-content-by-id.component.html',
  styleUrls: ['./web-content-by-id.component.css']
})
export class WebContentByIdComponent implements OnDestroy {

  subscriptions = new Array<Subscription>();
  webContent: WebContent | undefined;
  routing: VisitorRoutingService;

  constructor(private webContentService: VisitorWebContentService,
              private commentService: VisitorCommentService,
              private router: Router,
              private route: ActivatedRoute,
              private routingService: RoutingService,
              private visitorRouting: VisitorRoutingService) {

    this.routing = visitorRouting;

    router.events.subscribe(r => {
      if (r instanceof NavigationEnd) {

        const versionId = this.route.snapshot.params.id;

        // retrieve the WebContent
        this.subscriptions.push(
          this.webContentService.getOneByVersionId(versionId)
            .subscribe(value => {
              this.webContent = value;
            }, () => {
              // if WebContent not found
              this.routingService.goToPageNotFound();
            }));

      }
    });
  }


  ngOnDestroy(): void {
    UnsubscribeService.unsubscribeAll(this.subscriptions);
  }
}
