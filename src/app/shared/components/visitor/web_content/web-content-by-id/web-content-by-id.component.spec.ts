import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WebContentByIdComponent } from './web-content-by-id.component';

describe('WebContentByIdComponent', () => {
  let component: WebContentByIdComponent;
  let fixture: ComponentFixture<WebContentByIdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WebContentByIdComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WebContentByIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
