import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisitorSortWebContentsComponent } from './visitor-sort-web-contents.component';

describe('VisitorSortWebContentsComponent', () => {
  let component: VisitorSortWebContentsComponent;
  let fixture: ComponentFixture<VisitorSortWebContentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisitorSortWebContentsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisitorSortWebContentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
