import {Component, OnDestroy} from '@angular/core';
import {Subscription} from 'rxjs';
import {UnsubscribeService} from '../../../../utilities/unsubscribe.service';
import {VisitorRoutingService} from '../../../../../core/services/utilities/visitor/visitor-routing.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {VisitorWebContentService} from '../../../../../core/services/visitor/visitor-web-content.service';
import {VisitorSortingItemsService} from '../../../../../core/services/visitor/visitor-sorting-items.service';
import {VisitorWebContentTypeService} from '../../../../../core/services/visitor/visitor-web-content-type.service';
import {VisitorWebContentCategoryService} from '../../../../../core/services/visitor/visitor-web-content-category.service';
import {VisitorUserService} from '../../../../../core/services/visitor/visitor-user.service';
import {WebContentType} from '../../../../models/dto/web-content-type';
import {WebContentCategory} from '../../../../models/dto/web-content-category';
import {User} from '../../../../models/dto/user';
import {VisitorWebContentOrderSorter} from '../../../../models/payload/request/visitor/visitor-web-content-order-sorter';

@Component({
  selector: 'app-visitor-sort-web-contents',
  templateUrl: './visitor-sort-web-contents.component.html',
  styleUrls: ['./visitor-sort-web-contents.component.css']
})
export class VisitorSortWebContentsComponent implements OnDestroy {

  subscriptions = new Array<Subscription>();

  routing: VisitorRoutingService;
  form: FormGroup;
  formMessage: string | undefined;


  typeSorters: Array<WebContentType> | undefined;
  categorySorters: Array<WebContentCategory> | undefined;
  editors: Array<User> | undefined;
  orderSorters: Array<VisitorWebContentOrderSorter> | undefined;


  urlTypeId: string | undefined;
  urlType: WebContentType | undefined;
  urlCategoryId: string | undefined;
  urlCategory: WebContentCategory | undefined;
  urlUserId: string | undefined;
  urlUser: User | undefined;
  urlSortId: string | undefined;
  urlSort: VisitorWebContentOrderSorter | undefined;


  formLoaded: boolean | undefined;
  displayForm: boolean | undefined;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private visitorWebContentService: VisitorWebContentService,
              private visitorSortingService: VisitorSortingItemsService,
              private visitorTypeService: VisitorWebContentTypeService,
              private  visitorCategoryService: VisitorWebContentCategoryService,
              private visitorUserService: VisitorUserService,
              private routingService: VisitorRoutingService,
              private formBuilder: FormBuilder) {
    this.routing = this.routingService;
    this.form = this.initForm();
    this.setPropertiesOnRouting();

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.setPropertiesOnRouting();
      }
    });
  }


  getSorters(): any {

    this.subscriptions.push(this.visitorTypeService.getWebContentSortingTypes()
      .subscribe(value => {
        this.typeSorters = value;
        // tslint:disable-next-line:triple-equals
        this.urlType = this.typeSorters?.find(t => t?.versionId == this.urlTypeId);
        this.setFormLoadedTrue();
      }));

    this.subscriptions.push(this.visitorCategoryService.getWebContentSortingCategories()
      .subscribe(value => {
        this.categorySorters = value;
        // tslint:disable-next-line:triple-equals
        this.urlCategory = this.categorySorters?.find(c => c?.versionId == this.urlCategoryId);
        this.setFormLoadedTrue();
      }));

    this.subscriptions.push(this.visitorUserService.getEditors()
      .subscribe(value => {
        this.editors = value;
        // tslint:disable-next-line:triple-equals
        this.urlUser = this.editors?.find(u => u?.id == this.urlUserId);
        this.setFormLoadedTrue();
      }));

    this.subscriptions.push(this.visitorSortingService.getOrderSortersItems()
      .subscribe(value => {
        this.orderSorters = value;
        // tslint:disable-next-line:triple-equals
        this.urlSort = this.orderSorters?.find(s => s?.id == this.urlSortId);
        this.setFormLoadedTrue();
      }));
  }

  setFormLoadedTrue(): any {
    if (this.typeSorters && this.categorySorters && this.editors && this.orderSorters) {
      this.form = this.initForm();
      this.formLoaded = true;
      this.routing.routing = false;
    }
  }

  setPropertiesOnRouting(): any {

    this.formLoaded = undefined;
    this.formMessage = undefined;
    this.displayForm = undefined;
    this.urlType = undefined;
    this.urlCategory = undefined;
    this.urlUser = undefined;
    this.urlSort = undefined;

    this.urlTypeId = this.route.snapshot.params?.typeId;
    this.urlCategoryId = this.route.snapshot.params?.catId;
    this.urlUserId = this.route.snapshot.params?.userId;
    this.urlSortId = this.route.snapshot.params?.sort;
    if (this.isNull(this.urlSortId)) {
      this.urlSortId = 'publishing-date-desc';
    }
    this.getSorters();

  }

  onClickDisplayForm(): any {
    this.displayForm = !this.displayForm;
    if (this.formMessage) {
      this.formMessage = undefined;
    }
  }

  isNull(object: any): boolean {
    return object === null || object === '' || object === undefined;
  }

  onSubmit(): any {

    const formValues = this.form.value;
    const formTypeId = formValues?.type?.id;
    const formTypeTitle = formValues?.type?.title;
    const formCatId = formValues?.category?.id;
    const formCatTitle = formValues?.category?.title;
    const formUserId = formValues?.user?.id;
    const formUserTitle = formValues?.user?.pseudo;
    let formSortId = formValues?.sort?.id;

    if (this.isNull(formSortId)) {
      formSortId = 'publishing-date-desc';
    }

    if (this.isNull(formTypeId) && this.isNull(formCatId) && this.isNull(formUserId)) {
      // tslint:disable-next-line:triple-equals
      if (this.urlSortId == formSortId) {
        console.log('nothing chosen and same order');
        this.formMessage = 'Choix déjà affiché';
        // this.displayForm = false;
        return;
      } else {
        if (!this.isNull(this.urlType) && this.isNull(this.urlCategory) && this.isNull(this.urlUser)) {
          this.routingService.goToTypeAndSort(this?.urlType?.versionId, this?.urlType?.title, formSortId);
        } else if (this.isNull(this.urlType) && !this.isNull(this.urlCategory) && this.isNull(this.urlUser)) {
          this.routingService.goToCategoryAndSort(this.urlCategory?.versionId, this.urlCategory?.title, formSortId);
        } else if (this.isNull(this.urlType) && this.isNull(this.urlCategory) && !this.isNull(this.urlUser)) {
          this.routingService.goToUserContentsAndSort(this.urlUser?.id, this?.urlUser?.pseudo, formSortId);
        } else if (!this.isNull(this.urlType) && !this.isNull(this.urlCategory) && this.isNull(this.urlUser)) {
          this.routingService.goToTypeAndCategoryAndSort(this?.urlType?.versionId, this.urlType?.title,
            this.urlCategory?.versionId, this.urlCategory?.title, formSortId);
        } else if (!this.isNull(this.urlType) && this.isNull(this.urlCategory) && !this.isNull(this.urlUser)) {
          this.routingService.goToUserAndTypeAndSort(this.urlUser?.id, this.urlUser?.pseudo,
            this?.urlType?.versionId, this.urlType?.title, formSortId);
        } else if (this.isNull(this.urlType) && !this.isNull(this.urlCategory) && !this.isNull(this.urlUser)) {
          this.routingService.goToUserAndCategoryAndSort(this.urlUser?.id, this?.urlUser?.pseudo,
            this.urlCategory?.versionId, this?.urlCategory?.title, formSortId);
        } else if (!this.isNull(this.urlType) && !this.isNull(this.urlCategory) && !this.isNull(this.urlUser)) {
          this.routingService.goToUserAndTypeAndCategoryAndSort(this.urlUser?.id, this.urlUser?.pseudo,
            this.urlType?.versionId, this.urlType?.title, this.urlCategory?.versionId, this.urlCategory?.title, formSortId);
        } else if (this.isNull(this.urlType) && this.isNull(this.urlCategory) && this.isNull(this.urlUser)) {
          console.log('nothing chosen and empty url');
          this.formMessage = 'Choix déjà affiché';
          return;
        }
      }
      // tslint:disable-next-line:triple-equals max-line-length
    } else if (this.urlTypeId == formTypeId && this.urlCategoryId == formCatId && this.urlUserId == formUserId && this.urlSortId == formSortId) {
      // this.displayForm = false;
      console.log('nothing new chosen, identical url and form items');
      this.formMessage = 'Choix déjà affiché';
      return;
    } else {
      if (!this.isNull(formTypeId) && this.isNull(formCatId) && this.isNull(formUserId)) {
        this.routingService.goToTypeAndSort(formTypeId, formTypeTitle, formSortId);
      } else if (this.isNull(formTypeId) && !this.isNull(formCatId) && this.isNull(formUserId)) {
        this.routingService.goToCategoryAndSort(formCatId, formCatTitle, formSortId);
      } else if (this.isNull(formTypeId) && this.isNull(formCatId) && !this.isNull(formUserId)) {
        this.routingService.goToUserContentsAndSort(formUserId, formUserTitle, formSortId);
      } else if (!this.isNull(formTypeId) && !this.isNull(formCatId) && this.isNull(formUserId)) {
        this.routingService.goToTypeAndCategoryAndSort(formTypeId, formTypeTitle, formCatId, formCatTitle, formSortId);
      } else if (!this.isNull(formTypeId) && this.isNull(formCatId) && !this.isNull(formUserId)) {
        this.routingService.goToUserAndTypeAndSort(formUserId, formUserTitle, formTypeId, formTypeTitle, formSortId);
      } else if (this.isNull(formTypeId) && !this.isNull(formCatId) && !this.isNull(formUserId)) {
        this.routingService.goToUserAndCategoryAndSort(formUserId, formUserTitle, formCatId, formCatTitle, formSortId);
      } else if (!this.isNull(formTypeId) && !this.isNull(formCatId) && !this.isNull(formUserId)) {
        this.routingService.goToUserAndTypeAndCategoryAndSort(formUserId, formUserTitle, formTypeId,
          formTypeTitle, formCatId, formCatTitle, formSortId);
      }
    }
  }

  initForm(): FormGroup {
    return this.formBuilder.group({
      type: [this.urlType],
      category: [this.urlCategory],
      user: [this.urlUser],
      sort: [this.urlSort]
    });
  }

  ngOnDestroy(): void {
    UnsubscribeService.unsubscribeAll(this.subscriptions);
  }

}
