import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {UnsubscribeService} from '../../../utilities/unsubscribe.service';
import {VisitorRoutingService} from '../../../../core/services/utilities/visitor/visitor-routing.service';
import {VisitorCommentService} from '../../../../core/services/visitor/visitor-comment.service';
import {NavigationEnd, Router} from '@angular/router';
import {Comment} from '../../../models/dto/comment';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnDestroy, OnInit {

  subscriptions = new Array<Subscription>();

  comments: Array<Comment> | undefined;
  routing: VisitorRoutingService;


  @Input()
  wcVersionId: number | undefined;

  constructor(private commentService: VisitorCommentService,
              private visitorRouting: VisitorRoutingService,
              private router: Router) {

    this.routing = visitorRouting;

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.getComments();
      }
    });


  }

  ngOnInit(): void {
    this.getComments();

  }


  getComments(): void {

    this.subscriptions.push(
      this.commentService.getWebContentParentComments(this.wcVersionId)
        .subscribe(parentComments => {
          this.comments = parentComments;
          parentComments?.forEach((c: any) => console.log('parent comment ', c));

          this.comments?.forEach(c => {
            if (c.publishingDate != null) {
              this.subscriptions.push(
                this.commentService.getWebContentChildrenComments(c.id)
                  .subscribe(childrenComments => {
                    c.childrenComments = childrenComments;
                    console.log('children comments ', childrenComments);
                  }));
            }
          });
        }));

  }


  displayReactions(event: any): any {
    const element = document.getElementById(event?.target?.value);
    if (element) {
      element.classList.toggle('hidden');
      if (element.classList.contains('hidden')) {
        event.target.textContent = 'Afficher réactions';
      } else {
        event.target.textContent = 'Masquer réactions';
      }
    }
  }

  ngOnDestroy(): void {
    UnsubscribeService.unsubscribeAll(this.subscriptions);
  }

}
