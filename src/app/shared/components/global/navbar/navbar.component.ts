import {Component, Input} from '@angular/core';

import {Router} from '@angular/router';
import {VisitorRoutingService} from '../../../../core/services/utilities/visitor/visitor-routing.service';
import {WebContentType} from '../../../models/dto/web-content-type';
import {WebContentCategory} from '../../../models/dto/web-content-category';
import {LoginJwtResponse} from '../../../models/payload/response/login/login-jwt-response';
import {TokenStorageService} from '../../../../core/services/signup_login/token-storage.service';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {


  @Input()
  webContentTypesNavbarItems: Array<WebContentType> | undefined;

  @Input()
  categories: Array<WebContentCategory> | undefined;

  @Input()
  sessionUser: LoginJwtResponse | undefined;

  isUser: boolean | undefined;


  routing: VisitorRoutingService;
  tokenStorage: TokenStorageService;

  constructor(private router: Router,
              private routingService: VisitorRoutingService,
              private tokenStorageService: TokenStorageService) {
    this.routing = this.routingService;
    this.tokenStorage = this.tokenStorageService;
  }




}
