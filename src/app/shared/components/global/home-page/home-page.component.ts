import {Component, OnDestroy} from '@angular/core';
import {VisitorWebContentService} from '../../../../core/services/visitor/visitor-web-content.service';
import {Subscription} from 'rxjs';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {UnsubscribeService} from '../../../utilities/unsubscribe.service';
import {RoutingService} from '../../../utilities/routing.service';
import {WebContent} from '../../../models/dto/web-content';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnDestroy {


  webContent: WebContent | undefined;
  getWebContentSubscription: Subscription | undefined;


  constructor(private webContentService: VisitorWebContentService,
              private router: Router,
              private route: ActivatedRoute,
              private routingService: RoutingService) {

    router.events.subscribe(r => {
      if (r instanceof NavigationEnd) {


        console.log('received ', this.router.getCurrentNavigation()?.extras.state);
        console.log('received from ', this.router.getCurrentNavigation()?.extras?.state?.fromCharte);


        this.getWebContentSubscription = this.webContentService.getHomePage()
          .subscribe(value => {
            this.webContent = value;
          }, error => {
            console.log('error ', error);
            this.routingService.goToPageNotFound();
          });
      }
    });
  }


  ngOnDestroy(): void {
    UnsubscribeService.unsubscribe(this.getWebContentSubscription);
  }

}
