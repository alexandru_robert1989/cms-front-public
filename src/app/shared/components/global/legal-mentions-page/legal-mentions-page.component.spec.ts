import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LegalMentionsPageComponent } from './legal-mentions-page.component';

describe('LegalMentionsPageComponent', () => {
  let component: LegalMentionsPageComponent;
  let fixture: ComponentFixture<LegalMentionsPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LegalMentionsPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LegalMentionsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
