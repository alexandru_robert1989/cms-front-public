import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {VisitorWebContentService} from '../../../../core/services/visitor/visitor-web-content.service';
import {NavigationEnd, Router} from '@angular/router';
import {RoutingService} from '../../../utilities/routing.service';
import {UnsubscribeService} from '../../../utilities/unsubscribe.service';
import {WebContent} from '../../../models/dto/web-content';

@Component({
  selector: 'app-legal-mentions-page',
  templateUrl: './legal-mentions-page.component.html',
  styleUrls: ['./legal-mentions-page.component.css']
})
export class LegalMentionsPageComponent implements OnDestroy {

  webContent: WebContent | undefined;
  getWebContentSubscription: Subscription | undefined;


  constructor(private webContentService: VisitorWebContentService,
              private router: Router,
              private routingService: RoutingService) {

    router.events.subscribe(r => {

      if (r instanceof NavigationEnd) {

      //  console.log('received ', this.router.getCurrentNavigation()?.extras.state);

        this.getWebContentSubscription = this.webContentService.getLegalMentionsPage()
          .subscribe(value => {
            this.webContent = value;
          }, () => {
            this.routingService.goToPageNotFound();
          });
      }
    });
  }


  ngOnDestroy(): void {
    UnsubscribeService.unsubscribe(this.getWebContentSubscription);
  }

}
